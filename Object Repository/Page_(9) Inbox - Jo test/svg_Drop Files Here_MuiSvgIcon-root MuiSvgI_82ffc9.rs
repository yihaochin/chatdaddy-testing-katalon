<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>svg_Drop Files Here_MuiSvgIcon-root MuiSvgI_82ffc9</name>
   <tag></tag>
   <elementGuidId>c63ef70c-7b7c-4fc6-892a-d31a7a4b9200</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Drop Files Here'])[1]/following::*[name()='svg'][2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>svg.MuiSvgIcon-root.MuiSvgIcon-fontSizeMedium.css-1cqdur9</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>svg</value>
      <webElementGuid>da47eb16-f929-41db-8259-43ec88469005</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiSvgIcon-root MuiSvgIcon-fontSizeMedium css-1cqdur9</value>
      <webElementGuid>ff2bd972-8ce8-4928-b411-7faeeba2d334</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>focusable</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>23528165-b034-41b4-a174-882155ebef4e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-hidden</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>d252cd21-954c-4810-9c60-91c19a3c98d1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>viewBox</name>
      <type>Main</type>
      <value>0 0 24 24</value>
      <webElementGuid>519e4594-eaec-417d-af32-0fbd6c53bd83</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-testid</name>
      <type>Main</type>
      <value>CloseFullscreenIcon</value>
      <webElementGuid>1eea8e14-e200-4870-ad8f-595cf8605e52</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;hydrated&quot;]/body[1]/div[@class=&quot;MuiPopover-root MuiModal-root css-jp7szo&quot;]/div[@class=&quot;MuiPaper-root MuiPaper-elevation MuiPaper-rounded MuiPaper-elevation8 MuiPopover-paper css-1dmzujt&quot;]/div[@class=&quot;MuiGrid-root css-18pq57u&quot;]/div[@class=&quot;MuiGrid-root MuiGrid-container css-1sb7obf&quot;]/span[1]/button[@class=&quot;MuiButtonBase-root MuiIconButton-root MuiIconButton-sizeMedium css-g5fp96&quot;]/svg[@class=&quot;MuiSvgIcon-root MuiSvgIcon-fontSizeMedium css-1cqdur9&quot;]</value>
      <webElementGuid>08117f3d-1c34-4510-a004-94b510061801</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Drop Files Here'])[1]/following::*[name()='svg'][2]</value>
      <webElementGuid>b5a87059-b6f3-4da6-9a6c-40710b288683</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='mic'])[1]/following::*[name()='svg'][3]</value>
      <webElementGuid>4dc2ca7e-053c-40bb-95f6-7cadf4c2daf7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Train your own AI ChatBot'])[1]/preceding::*[name()='svg'][1]</value>
      <webElementGuid>eedb16e9-ac6e-486c-8cd5-133bb43a7d13</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='done'])[3]/preceding::*[name()='svg'][2]</value>
      <webElementGuid>65a93060-995a-46ef-a036-61e5a090f95f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
