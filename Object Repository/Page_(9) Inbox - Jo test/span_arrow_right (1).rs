<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_arrow_right (1)</name>
   <tag></tag>
   <elementGuidId>b91d0697-9831-4725-8665-94a73f3af4a4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div[3]/div[3]/div/div[3]/div[2]/div[2]/div/div[4]/div/span/div/div/div/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>span.material-symbols-rounded.MuiBox-root.css-159niyq</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>7c29349c-3f1d-433d-8198-40bc6313c909</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>material-symbols-rounded MuiBox-root css-159niyq</value>
      <webElementGuid>e6049fab-185c-4a37-903a-6a8a3f6545d5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>arrow_right</value>
      <webElementGuid>bd95f93b-4128-4d4c-95a4-72a8ad47480b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;MuiGrid-root MuiGrid-container MuiGrid-direction-xs-column MuiGrid-wrap-xs-nowrap css-5u791s&quot;]/div[@class=&quot;MuiGrid-root css-qrdc5g&quot;]/div[@class=&quot;MuiGrid-root css-1r3ro9p&quot;]/div[@class=&quot;MuiBox-root css-13tphjr&quot;]/div[@class=&quot;MuiGrid-root MuiGrid-container MuiGrid-wrap-xs-nowrap css-1112fw4&quot;]/div[@class=&quot;MuiGrid-root css-1fghw63&quot;]/div[@class=&quot;MuiGrid-root css-1eiz13t&quot;]/div[@class=&quot;MuiGrid-root css-7j22ch&quot;]/div[@class=&quot;MuiGrid-root css-1c8gsuk&quot;]/span[@class=&quot;MuiTypography-root MuiTypography-baseMedium css-1oz3psz&quot;]/div[@class=&quot;MuiCollapse-root MuiCollapse-horizontal MuiCollapse-entered css-1cijpyl&quot;]/div[@class=&quot;MuiCollapse-wrapper MuiCollapse-horizontal css-164swfl&quot;]/div[@class=&quot;MuiCollapse-wrapperInner MuiCollapse-horizontal css-1mziwkx&quot;]/span[@class=&quot;material-symbols-rounded MuiBox-root css-159niyq&quot;]</value>
      <webElementGuid>8583d572-a022-44ad-ab08-e7fb9b3b1c01</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div[3]/div[3]/div/div[3]/div[2]/div[2]/div/div[4]/div/span/div/div/div/span</value>
      <webElementGuid>0e8aa24e-c10b-484f-b1f6-094784089431</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='May 9, 2024 at 3:32 PM'])[2]/following::span[3]</value>
      <webElementGuid>0914fe1a-bd57-49dd-8cff-435d3f5f5490</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='grid_view'])[1]/preceding::span[1]</value>
      <webElementGuid>c0f3ea91-c00a-4781-8675-a04cfa47dd29</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='add'])[4]/preceding::span[2]</value>
      <webElementGuid>1e24c7ef-151c-47c5-8b42-f4833717c4de</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/span/div/div/div/span</value>
      <webElementGuid>445631c9-bc2a-4ffb-9a7e-2ebf7e8e8844</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'arrow_right' or . = 'arrow_right')]</value>
      <webElementGuid>7de97f0d-feca-4bcb-a0de-e2ec745550ad</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
