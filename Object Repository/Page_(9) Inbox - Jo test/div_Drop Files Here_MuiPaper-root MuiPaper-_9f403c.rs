<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Drop Files Here_MuiPaper-root MuiPaper-_9f403c</name>
   <tag></tag>
   <elementGuidId>3b4248d8-e63d-4fc5-a706-7247c73ed881</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Drop Files Here'])[1]/following::div[13]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.MuiPaper-root.MuiPaper-25.MuiPaper-rounded.MuiPopover-paper.css-rfalj3</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>8fab0d41-b630-4bfb-8585-0b65878fc3bd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiPaper-root MuiPaper-25 MuiPaper-rounded MuiPopover-paper css-rfalj3</value>
      <webElementGuid>4e801513-bfe5-4394-a132-795b221d1a8d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>-1</value>
      <webElementGuid>6216fac1-5cd3-435f-999b-12e9cd503bb7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;hydrated&quot;]/body[1]/div[@class=&quot;MuiPopover-root MuiModal-root css-jp7szo&quot;]/div[@class=&quot;MuiPaper-root MuiPaper-25 MuiPaper-rounded MuiPopover-paper css-rfalj3&quot;]</value>
      <webElementGuid>415797b7-122e-4daf-be8b-b59f504c8e15</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Drop Files Here'])[1]/following::div[13]</value>
      <webElementGuid>fa6ca53a-32cb-4650-8c45-190717a414d6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='expand_more'])[13]/following::div[13]</value>
      <webElementGuid>bff88cc1-c5c0-47f2-a7fc-158d7302f6a8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/div[3]</value>
      <webElementGuid>1edc6832-59cb-4029-a894-6ff8f9c85922</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
