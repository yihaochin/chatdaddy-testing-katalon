<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>textarea_add_complex-text-area (7)</name>
   <tag></tag>
   <elementGuidId>b3fd6f11-c53c-45c6-a697-1f674082a72c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//textarea[@id='complex-text-area']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#complex-text-area</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>textarea</value>
      <webElementGuid>a9146e29-29ac-44c2-8678-a29e4e726e18</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>complex-text-area</value>
      <webElementGuid>91fc92f1-0c8c-4e58-abcb-cc58db04cd8c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Type '/' to start a Message Flow! To apply AI modifications to your text, select it!</value>
      <webElementGuid>290cf96f-da6e-469c-9d75-717a13b99dbf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;complex-text-area&quot;)</value>
      <webElementGuid>0b32e3ab-bcf9-4bc2-9b67-772579afd674</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//textarea[@id='complex-text-area']</value>
      <webElementGuid>a1ce33d1-bc31-4470-8601-f4772862740c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div[3]/div[3]/div/div[3]/div[2]/div/div[2]/div[2]/div/div/div[2]/div/div/textarea</value>
      <webElementGuid>74d93e6c-8367-4e72-8eb4-eac629dceae5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//textarea</value>
      <webElementGuid>81643123-0947-46cc-8b98-f02e5b46e32a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//textarea[@id = 'complex-text-area' and @placeholder = concat(&quot;Type &quot; , &quot;'&quot; , &quot;/&quot; , &quot;'&quot; , &quot; to start a Message Flow! To apply AI modifications to your text, select it!&quot;)]</value>
      <webElementGuid>25491fe1-a182-42d8-acb2-01774f0a277e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
