<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Single Product 2 50</name>
   <tag></tag>
   <elementGuidId>da8cce09-63fc-4371-b812-afb20cfbd125</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='​'])[4]/following::div[5]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'MuiGrid-root css-88e6t3' and (text() = 'Single Product 2₱ 50' or . = 'Single Product 2₱ 50')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.MuiGrid-root.css-88e6t3</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>1f56dc8e-968f-42e9-8a12-2f7d66a8cdc3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiGrid-root css-88e6t3</value>
      <webElementGuid>bd9b7d78-77bc-443e-a6ed-4a1dabd9b60f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Single Product 2₱ 50</value>
      <webElementGuid>40fda6a2-a468-4d5b-8510-125d1fc22dc5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;hydrated&quot;]/body[1]/div[@class=&quot;MuiPopover-root MuiModal-root css-jp7szo&quot;]/div[@class=&quot;MuiPaper-root MuiPaper-elevation MuiPaper-rounded MuiPaper-elevation8 MuiPopover-paper css-r6ceqw&quot;]/div[@class=&quot;MuiGrid-root css-izigw9&quot;]/div[@class=&quot;MuiGrid-root product-scrollable-container css-iu6kj1&quot;]/div[@class=&quot;infinite-scroll-component__outerdiv&quot;]/div[@class=&quot;infinite-scroll-component&quot;]/div[@class=&quot;MuiGrid-root css-izigw9&quot;]/div[@class=&quot;MuiGrid-root css-88e6t3&quot;]</value>
      <webElementGuid>b5a55754-ad53-473b-a757-fe01a57c657d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='​'])[4]/following::div[5]</value>
      <webElementGuid>19604e4e-56a3-4eb2-9fdb-70a4dd4c8580</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='search'])[3]/following::div[5]</value>
      <webElementGuid>bb677672-6d68-4e8f-a859-c5677f6f60d2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/div[3]/div/div[2]/div/div/div/div</value>
      <webElementGuid>273f4960-850b-43a9-b9c8-acec1cc43395</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'Single Product 2₱ 50' or . = 'Single Product 2₱ 50')]</value>
      <webElementGuid>299f5ad7-47f4-44d8-b339-9fc249e72cbf</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
