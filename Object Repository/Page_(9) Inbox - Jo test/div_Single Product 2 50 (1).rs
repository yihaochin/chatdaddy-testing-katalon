<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Single Product 2 50 (1)</name>
   <tag></tag>
   <elementGuidId>e1093ee5-e7ce-429a-ba91-65aa264d640a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='​'])[4]/following::div[5]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.MuiGrid-root.css-88e6t3</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>526cce09-278a-47f8-b369-a978c795d6cc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiGrid-root css-88e6t3</value>
      <webElementGuid>7ba5e9db-4cfe-45d8-ad17-505c05718177</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Single Product 2₱ 50</value>
      <webElementGuid>1eafbc1a-4e23-4568-8cac-c0445c6e3bfa</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;hydrated&quot;]/body[1]/div[@class=&quot;MuiPopover-root MuiModal-root css-jp7szo&quot;]/div[@class=&quot;MuiPaper-root MuiPaper-elevation MuiPaper-rounded MuiPaper-elevation8 MuiPopover-paper css-r6ceqw&quot;]/div[@class=&quot;MuiGrid-root css-izigw9&quot;]/div[@class=&quot;MuiGrid-root product-scrollable-container css-iu6kj1&quot;]/div[@class=&quot;infinite-scroll-component__outerdiv&quot;]/div[@class=&quot;infinite-scroll-component&quot;]/div[@class=&quot;MuiGrid-root css-izigw9&quot;]/div[@class=&quot;MuiGrid-root css-88e6t3&quot;]</value>
      <webElementGuid>142a1be0-3fa0-40a3-acb8-9999e70a49f9</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='​'])[4]/following::div[5]</value>
      <webElementGuid>4a96c9bc-fa95-404e-b35d-0e925c2256e9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='search'])[3]/following::div[5]</value>
      <webElementGuid>f7fdc4b4-5b26-4f1d-b585-ac505919d596</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/div[3]/div/div[2]/div/div/div/div</value>
      <webElementGuid>8915fe02-2448-409b-9f75-c775e085fc45</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'Single Product 2₱ 50' or . = 'Single Product 2₱ 50')]</value>
      <webElementGuid>32595a0e-2ed6-4a3f-8b2a-333488153579</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
