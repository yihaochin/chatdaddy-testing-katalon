<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Jo test (20) (1)</name>
   <tag></tag>
   <elementGuidId>0791d377-f9f9-4abd-aba7-0d49628acd41</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div[3]/div[3]/div/div[2]/div[3]/div/div[2]/div/a[2]/div[2]/div/div/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>8378b530-6255-4624-81e7-3144ae6e6161</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiTypography-root MuiTypography-baseMedium css-1nchl2b</value>
      <webElementGuid>0e1a3c74-30ca-482f-8e91-ff25c6af8fa2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Jo test</value>
      <webElementGuid>26fb771b-b2b2-44ed-a9a8-1b62b245d654</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Jo test</value>
      <webElementGuid>700cf61d-1d49-46cd-a24d-970ec485e756</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;MuiGrid-root MuiGrid-container MuiGrid-direction-xs-column MuiGrid-wrap-xs-nowrap css-5u791s&quot;]/div[@class=&quot;MuiGrid-root css-qrdc5g&quot;]/div[@class=&quot;MuiGrid-root css-1r3ro9p&quot;]/div[@class=&quot;MuiGrid-root css-148h8t7&quot;]/div[@class=&quot;MuiGrid-root css-12u48ha&quot;]/div[1]/div[2]/div[1]/a[@class=&quot;MuiGrid-root css-128ea43&quot;]/div[@class=&quot;MuiGrid-root css-1vlsgix&quot;]/div[@class=&quot;MuiGrid-root css-1mr0hd4&quot;]/div[@class=&quot;MuiGrid-root css-1trzygj&quot;]/span[@class=&quot;MuiTypography-root MuiTypography-baseMedium css-1nchl2b&quot;]</value>
      <webElementGuid>f9548620-c407-436e-980a-43b66765ab0b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div[3]/div[3]/div/div[2]/div[3]/div/div[2]/div/a[2]/div[2]/div/div/span</value>
      <webElementGuid>d4ed5736-5199-4a75-a1ed-95463ff06623</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='account_circle'])[3]/following::span[3]</value>
      <webElementGuid>eb153529-9da4-4351-91a2-90bca2f41fef</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='expand_more'])[1]/following::span[6]</value>
      <webElementGuid>8b583c26-08df-4301-89a6-5a233e9ac768</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Add Note'])[1]/preceding::span[2]</value>
      <webElementGuid>f526b9ac-d376-4469-9a7f-db68e3c9806c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='VIP'])[1]/preceding::span[3]</value>
      <webElementGuid>d79621a7-d614-4737-9375-ca1419b43502</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Jo test']/parent::*</value>
      <webElementGuid>f38f8d8e-9273-4a02-84cd-4977142c4f2c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a[2]/div[2]/div/div/span</value>
      <webElementGuid>105bb640-86ed-4643-bc33-3f70c22b3fcf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[@title = 'Jo test' and (text() = 'Jo test' or . = 'Jo test')]</value>
      <webElementGuid>72a3e796-6f2c-4eba-a86a-d792412c3859</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
