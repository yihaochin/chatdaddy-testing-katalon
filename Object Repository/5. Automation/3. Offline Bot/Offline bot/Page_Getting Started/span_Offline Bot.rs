<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Offline Bot</name>
   <tag></tag>
   <elementGuidId>6ab81b15-7087-4a99-b14f-e98cd5ff6202</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Message Flows'])[2]/following::span[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>a.MuiGrid-root.css-1vo1ped > span.MuiTypography-root.MuiTypography-baseMedium.css-57mpad</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorCollection>
      <entry>
         <key>SMART_LOCATOR</key>
         <value>internal:role=link[name=&quot;Offline Bot&quot;i]</value>
      </entry>
   </smartLocatorCollection>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>22bf66c3-12de-4538-8990-21eb3bc3872f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiTypography-root MuiTypography-baseMedium css-57mpad</value>
      <webElementGuid>49b59717-fa73-4e18-ba4a-33915a3cc60c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Offline Bot</value>
      <webElementGuid>af872106-3b00-4d2e-9311-afd2c226370a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;hydrated&quot;]/body[1]/div[@class=&quot;base-Popper-root css-b82va4&quot;]/a[@class=&quot;MuiGrid-root css-1vo1ped&quot;]/span[@class=&quot;MuiTypography-root MuiTypography-baseMedium css-57mpad&quot;]</value>
      <webElementGuid>c10f7118-91c5-45e5-8147-f5406b3842c4</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Message Flows'])[2]/following::span[1]</value>
      <webElementGuid>319db162-592e-41b1-9e4e-8b8a6699495d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Automation'])[1]/following::span[2]</value>
      <webElementGuid>8049efc4-97fe-4389-a42a-0f83eda6c564</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Broadcasts'])[1]/preceding::span[1]</value>
      <webElementGuid>fdb852a7-ac3a-4031-9b38-6a67afe8f600</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Keyword Reply'])[1]/preceding::span[2]</value>
      <webElementGuid>3006f5d3-a67a-4734-87eb-21bf936c901b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Offline Bot']/parent::*</value>
      <webElementGuid>2db30c56-8db2-41ed-b4b4-3032a39b6eb9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a[2]/span</value>
      <webElementGuid>bdbcb54d-51f5-4a8b-a8d3-f03722cee3ae</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Offline Bot' or . = 'Offline Bot')]</value>
      <webElementGuid>f7517e7a-b1eb-4b1d-ab16-519048c79b04</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
