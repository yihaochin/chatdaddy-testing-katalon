<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Password_password</name>
   <tag></tag>
   <elementGuidId>33004b0c-c9f6-489b-b6e6-c48c88b68683</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'MuiInputBase-input MuiOutlinedInput-input css-1rpfghe']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id=':r2:']</value>
      </entry>
      <entry>
         <key>CSS</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorCollection>
      <entry>
         <key>SMART_LOCATOR</key>
         <value>internal:attr=[placeholder=&quot;••••••••••••&quot;i]</value>
      </entry>
   </smartLocatorCollection>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>fee950ca-84ec-489b-ae50-c5a115a24af7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-invalid</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>c90e4f3e-4af2-4f31-bb3f-134729c0111d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>:r2:</value>
      <webElementGuid>15173027-4176-42d6-a2c7-308f9779d2d9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>password</value>
      <webElementGuid>c2d73744-0db1-40a4-ba53-cf3ecde91fbb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>••••••••••••</value>
      <webElementGuid>70b643ea-4edc-40cc-85cf-ff20abb714a2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>password</value>
      <webElementGuid>d6f73c6d-3f75-425e-aa27-9f4e38ebe3cf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiInputBase-input MuiOutlinedInput-input css-1rpfghe</value>
      <webElementGuid>1c98cf56-15db-4d75-a46e-68b462c13278</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;:r2:&quot;)</value>
      <webElementGuid>ca7ca935-fc27-465e-bb9f-2af45f62ca3f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>d</value>
      <webElementGuid>2b5326b9-542a-4793-99b7-8674e3c7ae62</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id=':r2:']</value>
      <webElementGuid>627f4e26-34aa-4082-9c9a-34850569b3a7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div[2]/div/div/div[2]/div[2]/div/div/input</value>
      <webElementGuid>6aa0af5c-fafd-4bab-a934-89b792a5c7cb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/input</value>
      <webElementGuid>0ea313c0-4c1d-4c3d-a457-8146b8db8968</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = ':r2:' and @name = 'password' and @placeholder = '••••••••••••' and @type = 'password']</value>
      <webElementGuid>11c6f30b-a449-4f86-90ae-c8264efeb5a2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id=':r3:']</value>
      <webElementGuid>7a4a3910-0d93-4e72-aad8-b985984baa80</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = ':r3:' and @name = 'password' and @placeholder = '••••••••••••' and @type = 'password']</value>
      <webElementGuid>aab1bf7e-4bc1-4954-9505-a991ee701246</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id=':r4:']</value>
      <webElementGuid>d9de688d-2670-4d3a-b73a-32529d27400d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = ':r4:' and @name = 'password' and @placeholder = '••••••••••••' and @type = 'password']</value>
      <webElementGuid>091db4d8-0c06-4a6a-a82b-af367f60735d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id=':r6:']</value>
      <webElementGuid>bff1bfda-0c47-47df-a68b-4691a135a3ed</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = ':r6:' and @name = 'password' and @placeholder = '••••••••••••' and @type = 'password']</value>
      <webElementGuid>ea1ba6bd-71db-495f-bb30-6d1178b57c9f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
