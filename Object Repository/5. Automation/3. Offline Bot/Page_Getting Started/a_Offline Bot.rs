<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Offline Bot</name>
   <tag></tag>
   <elementGuidId>80121be8-a664-4458-845f-cafa0e2b1aa0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Trigger History'])[1]/following::a[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>a.MuiGrid-root.css-1vo1ped</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorCollection>
      <entry>
         <key>SMART_LOCATOR</key>
         <value>internal:role=link[name=&quot;Offline Bot&quot;i]</value>
      </entry>
   </smartLocatorCollection>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>55776b73-5741-4e7d-a0e4-14b2707e5859</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiGrid-root css-1vo1ped</value>
      <webElementGuid>58befb2a-e795-4f46-af32-c4ea3de29881</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/automation/offlineBot</value>
      <webElementGuid>cf43c778-082c-4482-86c9-4bb67b6190e9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Offline Bot</value>
      <webElementGuid>2cc20875-d1d7-48b1-a117-62336c91bddc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;hydrated&quot;]/body[1]/div[@class=&quot;base-Popper-root css-b82va4&quot;]/a[@class=&quot;MuiGrid-root css-1vo1ped&quot;]</value>
      <webElementGuid>5d5fe088-b5f0-41ae-a21f-6136fbc9946e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Trigger History'])[1]/following::a[1]</value>
      <webElementGuid>eabf980e-751c-4645-a448-9cb13e195c00</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Message Flows'])[2]/following::a[2]</value>
      <webElementGuid>068bdad9-838f-4db0-8436-c90aa2701c74</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Broadcasts'])[1]/preceding::a[1]</value>
      <webElementGuid>1e351a70-0ea8-4424-b884-44c9b33a7823</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/automation/offlineBot')]</value>
      <webElementGuid>8a6e00a0-7220-4013-b089-2a63703d91a7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a[3]</value>
      <webElementGuid>7856fb89-640b-49b8-9c65-9e19544040b2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/automation/offlineBot' and (text() = 'Offline Bot' or . = 'Offline Bot')]</value>
      <webElementGuid>f37b9e10-793b-4e54-9929-23340e738db5</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
