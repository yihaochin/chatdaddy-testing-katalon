<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Offline Bot (4)</name>
   <tag></tag>
   <elementGuidId>b45ea3a8-7b9e-47ac-849a-8405f06fea0e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'MuiGrid-root css-1vo1ped' and @href = '/automation/offlineBot' and (text() = 'Offline Bot' or . = 'Offline Bot')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Trigger History'])[1]/following::a[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>a.MuiGrid-root.css-1vo1ped</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorCollection>
      <entry>
         <key>SMART_LOCATOR</key>
         <value>internal:role=link[name=&quot;Offline Bot&quot;i]</value>
      </entry>
   </smartLocatorCollection>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>0a3f2d35-dede-407d-8587-736c7b24b7ed</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiGrid-root css-1vo1ped</value>
      <webElementGuid>1aef1d9e-618a-4e84-a324-c0565c3d345a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/automation/offlineBot</value>
      <webElementGuid>54b04214-932c-4e40-9eec-8e07401a8e80</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Offline Bot</value>
      <webElementGuid>e3533548-a020-4ff7-b598-aab065e8b7c5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;hydrated&quot;]/body[1]/div[@class=&quot;base-Popper-root css-b82va4&quot;]/a[@class=&quot;MuiGrid-root css-1vo1ped&quot;]</value>
      <webElementGuid>f2bdc6c2-991a-41fb-a897-8c98f3d52c6a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Trigger History'])[1]/following::a[1]</value>
      <webElementGuid>e47bf1df-71d4-411a-bde4-a3235e09ba32</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Message Flows'])[2]/following::a[2]</value>
      <webElementGuid>f2b7b07e-8720-404a-9822-76799db260af</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Broadcasts'])[1]/preceding::a[1]</value>
      <webElementGuid>6dd94d5f-bf80-4474-8e35-1b2268e7b88b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/automation/offlineBot')]</value>
      <webElementGuid>ea52ba2f-6cad-4517-a54d-75ac8383c4c2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a[3]</value>
      <webElementGuid>95943fdc-f410-4ad3-933a-36f42227fe60</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/automation/offlineBot' and (text() = 'Offline Bot' or . = 'Offline Bot')]</value>
      <webElementGuid>dfacd4d8-2dd2-4cac-af36-ee27ae516742</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Message Flows'])[2]/following::a[1]</value>
      <webElementGuid>70925c95-8a8b-4e37-becd-f574578e6032</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Automation'])[1]/following::a[2]</value>
      <webElementGuid>c94b6324-0cf0-44cc-83c6-ca3169e398da</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a[2]</value>
      <webElementGuid>25c1cbc6-f2b0-4fc7-8168-80f6fb1a6e0c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
