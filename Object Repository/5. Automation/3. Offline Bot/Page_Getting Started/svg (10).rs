<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>svg (10)</name>
   <tag></tag>
   <elementGuidId>14f9dfa6-912b-4a99-9c9e-da4f607e04f7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='fullscreen_exit'])[1]/preceding::*[name()='svg'][5]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.MuiGrid-root.css-1d2hfx > div.MuiGrid-root.css-wivtdb > div > svg</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorCollection>
      <entry>
         <key>SMART_LOCATOR</key>
         <value>div:nth-child(6) > .MuiGrid-root > div > svg</value>
      </entry>
   </smartLocatorCollection>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>svg</value>
      <webElementGuid>63f64fa4-014a-4d42-b931-ae78bd86abc3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xmlns</name>
      <type>Main</type>
      <value>http://www.w3.org/2000/svg</value>
      <webElementGuid>f1edad7e-210f-4c3a-8e61-01ef610a99fb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xmlns:xlink</name>
      <type>Main</type>
      <value>http://www.w3.org/1999/xlink</value>
      <webElementGuid>ab08263d-43b5-4abf-b00e-53765725279a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>viewBox</name>
      <type>Main</type>
      <value>0 0 500 500</value>
      <webElementGuid>fd3559e0-51da-4375-af71-997def6af243</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>width</name>
      <type>Main</type>
      <value>500</value>
      <webElementGuid>e5ad1c29-2ba6-4bce-88f0-9b8bd90647ad</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>height</name>
      <type>Main</type>
      <value>500</value>
      <webElementGuid>a8f9d0b7-ed4c-4ec2-ada2-472edd5153f2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>preserveAspectRatio</name>
      <type>Main</type>
      <value>xMidYMid meet</value>
      <webElementGuid>17f6985c-8f08-4936-b9aa-db6714a8ff18</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;MuiCollapse-root MuiCollapse-horizontal MuiCollapse-entered css-yhefzq&quot;]/div[@class=&quot;MuiCollapse-wrapper MuiCollapse-horizontal css-164swfl&quot;]/div[@class=&quot;MuiCollapse-wrapperInner MuiCollapse-horizontal css-1mziwkx&quot;]/div[@class=&quot;MuiBox-root css-gxs633&quot;]/div[@class=&quot;MuiGrid-root css-1d2hfx&quot;]/div[@class=&quot;MuiGrid-root css-wivtdb&quot;]/div[1]/svg[1]</value>
      <webElementGuid>d1c5605c-b859-472a-b712-8fff57a9c340</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='fullscreen_exit'])[1]/preceding::*[name()='svg'][5]</value>
      <webElementGuid>acafeb1d-8c33-40dd-bd22-7468c4a63f26</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
