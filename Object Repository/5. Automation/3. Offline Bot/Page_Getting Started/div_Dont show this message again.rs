<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Dont show this message again</name>
   <tag></tag>
   <elementGuidId>75b5274d-baeb-425d-bcf9-a5bd7d0424ba</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='For additional guidance and information, please refer to our Helpdoc:'])[1]/following::div[3]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.MuiGrid-root.css-boef7z</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorCollection>
      <entry>
         <key>SMART_LOCATOR</key>
         <value>internal:text=&quot;Don’t show this message again&quot;i</value>
      </entry>
   </smartLocatorCollection>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>eea91046-2b3c-4e09-82cb-caa4f6cd3eaf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiGrid-root css-boef7z</value>
      <webElementGuid>63ef5f0d-906e-41a4-93d6-3b41c52d963d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Don’t show this message again</value>
      <webElementGuid>1f287912-7fa9-41c1-a056-d28e93ed1954</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;hydrated&quot;]/body[1]/div[@class=&quot;MuiDialog-root MuiModal-root css-126xj0f&quot;]/div[@class=&quot;MuiDialog-container MuiDialog-scrollPaper css-ekeie0&quot;]/div[@class=&quot;MuiPaper-root MuiPaper-elevation MuiPaper-rounded MuiPaper-elevation24 MuiDialog-paper MuiDialog-paperScrollPaper MuiDialog-paperWidthMd css-1f8zc83&quot;]/div[@class=&quot;MuiGrid-root css-1l45pj0&quot;]/label[@class=&quot;MuiFormControlLabel-root MuiFormControlLabel-labelPlacementEnd css-1jaw3da&quot;]/span[@class=&quot;MuiTypography-root MuiTypography-body1 MuiFormControlLabel-label css-1l0poxv&quot;]/div[@class=&quot;MuiGrid-root css-boef7z&quot;]</value>
      <webElementGuid>92671a4b-2ab1-4600-a0d6-d1cef91ad1fb</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='For additional guidance and information, please refer to our Helpdoc:'])[1]/following::div[3]</value>
      <webElementGuid>50443c9d-ecf0-4357-b05b-6eb7d53c7743</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Contact Support'])[1]/preceding::div[1]</value>
      <webElementGuid>4232bf61-ab2d-4de0-ad4b-907af0155534</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Close'])[1]/preceding::div[1]</value>
      <webElementGuid>c43a5901-e62f-4802-97e9-84727ed899df</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Don’t show this message again']/parent::*</value>
      <webElementGuid>6574daec-94af-4d3d-a0fa-b522c1e98e09</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span[2]/div</value>
      <webElementGuid>ee0e65bd-69e1-4e06-849a-c4c779571a81</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'Don’t show this message again' or . = 'Don’t show this message again')]</value>
      <webElementGuid>d7af1531-e1a0-4e2c-a747-dee7fcb2918f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
