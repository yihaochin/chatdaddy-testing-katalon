<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>svg_Advanced Settings_MuiSvgIcon-root MuiSv_7688a7</name>
   <tag></tag>
   <elementGuidId>27e74cac-6a79-4b01-a45e-dda8834799d7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Advanced Settings'])[1]/following::*[name()='svg'][1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.MuiGrid-root.MuiGrid-container.MuiGrid-direction-xs-column.AdvancedSettings.css-1lz9gxs > div.MuiGrid-root.css-1oy0n29 > div.MuiGrid-root.css-avuqbz > div.MuiGrid-root.css-rfnosa > span > button.MuiButtonBase-root.MuiIconButton-root.MuiIconButton-sizeMedium.css-g5fp96 > svg.MuiSvgIcon-root.MuiSvgIcon-fontSizeMedium.css-lka7sr</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorCollection>
      <entry>
         <key>SMART_LOCATOR</key>
         <value>div:nth-child(5) > div > div > div:nth-child(2) > span > .MuiButtonBase-root</value>
      </entry>
   </smartLocatorCollection>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>svg</value>
      <webElementGuid>a0f16296-a678-4947-a5c4-fcb64663ebc0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiSvgIcon-root MuiSvgIcon-fontSizeMedium css-lka7sr</value>
      <webElementGuid>d23d1a21-6c1c-496f-abca-b5b3ca8f2f9c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>focusable</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>b362867b-cfd5-4361-a8b3-339746ef8d7b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-hidden</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>fb0e954e-2324-48b5-bb50-3304b7980926</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>viewBox</name>
      <type>Main</type>
      <value>0 0 24 24</value>
      <webElementGuid>f87d3e9f-5594-4014-8d25-0be512b12db8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-testid</name>
      <type>Main</type>
      <value>ExpandLessIcon</value>
      <webElementGuid>89f12049-3fae-4458-8e59-7f180ab055b0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;MuiGrid-root MuiGrid-container MuiGrid-direction-xs-column MuiGrid-wrap-xs-nowrap css-5u791s&quot;]/div[@class=&quot;MuiGrid-root MuiGrid-container MuiGrid-direction-xs-column css-1qrgjpa&quot;]/div[@class=&quot;MuiGrid-root MuiGrid-container css-nxpjbh&quot;]/div[@class=&quot;MuiGrid-root MuiGrid-container css-z7c6hm&quot;]/div[@class=&quot;MuiGrid-root MuiGrid-container MuiGrid-direction-xs-column css-k0hwar&quot;]/div[@class=&quot;MuiGrid-root MuiGrid-container css-yt88p2&quot;]/div[@class=&quot;MuiGrid-root MuiGrid-container MuiGrid-direction-xs-column MuiGrid-wrap-xs-nowrap css-1qcw6km&quot;]/div[@class=&quot;MuiGrid-root MuiGrid-container MuiGrid-direction-xs-column AdvancedSettings  css-1lz9gxs&quot;]/div[@class=&quot;MuiGrid-root css-1oy0n29&quot;]/div[@class=&quot;MuiGrid-root css-avuqbz&quot;]/div[@class=&quot;MuiGrid-root css-rfnosa&quot;]/span[1]/button[@class=&quot;MuiButtonBase-root MuiIconButton-root MuiIconButton-sizeMedium css-g5fp96&quot;]/svg[@class=&quot;MuiSvgIcon-root MuiSvgIcon-fontSizeMedium css-lka7sr&quot;]</value>
      <webElementGuid>6ee31b7b-8b68-433e-af0b-bf6b79797675</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Advanced Settings'])[1]/following::*[name()='svg'][1]</value>
      <webElementGuid>70846bde-5b63-4b76-8c6b-bd84ff6efde8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='content_copy'])[7]/following::*[name()='svg'][1]</value>
      <webElementGuid>fd4578e3-138b-4208-bea4-672a570acdb7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Mark Chat as Read if Offline Bot is Triggered'])[1]/preceding::*[name()='svg'][1]</value>
      <webElementGuid>15e742ab-e2eb-4ea7-bc81-193915c714db</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
