<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>img</name>
   <tag></tag>
   <elementGuidId>d9a194d9-13d7-46ad-9e4e-f690e374e0ec</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//img[contains(@src,'https://chatdaddy-media-store.s3.ap-east-1.amazonaws.com/51d1b9bd9da236db')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.MuiGrid-root.css-i4tef6 > img</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorCollection>
      <entry>
         <key>SMART_LOCATOR</key>
         <value>internal:role=img >> nth=0</value>
      </entry>
   </smartLocatorCollection>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>img</value>
      <webElementGuid>3092ffae-069a-4c6e-b3e6-8c518bc763e5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>src</name>
      <type>Main</type>
      <value>https://chatdaddy-media-store.s3.ap-east-1.amazonaws.com/51d1b9bd9da236db</value>
      <webElementGuid>1df316ee-8a6a-4dae-846b-3ba4b76728d8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;flow-selector-scrollable-target&quot;)/div[@class=&quot;MuiGrid-root css-8fmb2&quot;]/div[@class=&quot;MuiGrid-root MuiGrid-direction-xs-column css-vfscm3&quot;]/div[@class=&quot;MuiGrid-root css-1n2nk9&quot;]/div[@class=&quot;MuiGrid-root css-i4tef6&quot;]/img[1]</value>
      <webElementGuid>23a56801-63ae-4160-ba97-9b132a9286e1</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='flow-selector-scrollable-target']/div/div/div/div/img</value>
      <webElementGuid>b2a78f81-17b0-4db0-833f-92ab83914d74</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:img</name>
      <type>Main</type>
      <value>//img[contains(@src,'https://chatdaddy-media-store.s3.ap-east-1.amazonaws.com/51d1b9bd9da236db')]</value>
      <webElementGuid>6826d3fa-0a60-4360-bb9b-7f5dde129ffa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div/div/div/img</value>
      <webElementGuid>037c5e79-888f-4447-968a-db3e8c8d052a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//img[@src = 'https://chatdaddy-media-store.s3.ap-east-1.amazonaws.com/51d1b9bd9da236db']</value>
      <webElementGuid>5192a761-945e-437e-9f4b-0acf24a64859</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
