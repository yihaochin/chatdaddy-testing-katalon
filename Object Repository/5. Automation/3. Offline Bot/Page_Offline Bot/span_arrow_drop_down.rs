<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_arrow_drop_down</name>
   <tag></tag>
   <elementGuidId>d96f2100-9074-4043-baaa-3ddc53f85bcb</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div[3]/div[3]/div/div/div/div/div/div[2]/div[2]/div/div[3]/div/button/span[2]/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>span.material-symbols-rounded.MuiBox-root.css-1d42fir</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorCollection>
      <entry>
         <key>SMART_LOCATOR</key>
         <value>internal:role=button[name=&quot;1 arrow_drop_down&quot;i]</value>
      </entry>
   </smartLocatorCollection>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>6309234c-4699-4c93-8365-e6edc8a9b779</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>material-symbols-rounded MuiBox-root css-1d42fir</value>
      <webElementGuid>e02edc9a-27b8-4baa-8989-38e7c31032a1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>arrow_drop_down</value>
      <webElementGuid>cb354cd2-5e59-4dff-a861-b623298771a4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;MuiGrid-root MuiGrid-container MuiGrid-direction-xs-column MuiGrid-wrap-xs-nowrap css-5u791s&quot;]/div[@class=&quot;MuiGrid-root MuiGrid-container MuiGrid-direction-xs-column css-1qrgjpa&quot;]/div[@class=&quot;MuiGrid-root MuiGrid-container css-nxpjbh&quot;]/div[@class=&quot;MuiGrid-root MuiGrid-container css-z7c6hm&quot;]/div[@class=&quot;MuiGrid-root MuiGrid-container MuiGrid-direction-xs-column css-k0hwar&quot;]/div[@class=&quot;MuiGrid-root MuiGrid-container css-yt88p2&quot;]/div[@class=&quot;MuiGrid-root MuiGrid-container MuiGrid-direction-xs-column MuiGrid-wrap-xs-nowrap css-1qcw6km&quot;]/div[@class=&quot;MuiGrid-root MuiGrid-container MuiGrid-direction-xs-column Settings  css-1lz9gxs&quot;]/div[@class=&quot;MuiGrid-root css-1a47weo&quot;]/div[@class=&quot;MuiGrid-root MuiGrid-container MuiGrid-direction-xs-column css-1atv4i4&quot;]/div[@class=&quot;MuiGrid-root css-cwgml&quot;]/div[@class=&quot;MuiGrid-root MuiGrid-container MuiGrid-wrap-xs-nowrap css-us8rr4&quot;]/button[@class=&quot;MuiButtonBase-root MuiButton-root MuiButton-outlined MuiButton-outlinedSecondary MuiButton-sizeMedium MuiButton-outlinedSizeMedium MuiButton-colorSecondary MuiButton-root MuiButton-outlined MuiButton-outlinedSecondary MuiButton-sizeMedium MuiButton-outlinedSizeMedium MuiButton-colorSecondary css-t0e2zl&quot;]/span[@class=&quot;MuiButton-icon MuiButton-endIcon MuiButton-iconSizeMedium css-pt151d&quot;]/span[@class=&quot;material-symbols-rounded MuiBox-root css-1d42fir&quot;]</value>
      <webElementGuid>3bbf49c5-56e8-40a0-8f85-9fd5956b4371</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div[3]/div[3]/div/div/div/div/div/div[2]/div[2]/div/div[3]/div/button/span[2]/span</value>
      <webElementGuid>7cb25290-6ab7-4f51-9319-370d05f64d77</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Once Every'])[1]/following::span[3]</value>
      <webElementGuid>3347baa2-7e1b-4207-be41-e137d944e19e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Set conditions to how often offline bot is triggered.'])[1]/following::span[9]</value>
      <webElementGuid>9af8501b-19fa-41d2-bebc-c2c06cda9b87</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Hours'])[1]/preceding::span[4]</value>
      <webElementGuid>1106b288-dd87-442b-8693-3395cf97a6c6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='arrow_drop_down'])[2]/preceding::span[5]</value>
      <webElementGuid>31e759ad-07d1-428a-a633-bd97a7a81c29</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='arrow_drop_down']/parent::*</value>
      <webElementGuid>df1c6fa5-9e47-4c90-944a-cc424b405900</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span[2]/span</value>
      <webElementGuid>b153ad66-c42d-4832-9e52-e0f559318cec</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'arrow_drop_down' or . = 'arrow_drop_down')]</value>
      <webElementGuid>b61842ce-e901-494f-9cc6-55abe97c4be7</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
