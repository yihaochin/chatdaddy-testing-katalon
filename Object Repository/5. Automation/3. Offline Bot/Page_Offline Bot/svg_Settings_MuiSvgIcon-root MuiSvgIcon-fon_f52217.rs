<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>svg_Settings_MuiSvgIcon-root MuiSvgIcon-fon_f52217</name>
   <tag></tag>
   <elementGuidId>9659cf0c-c945-4de8-a4b6-7f6a2b51025a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Settings'])[1]/following::*[name()='svg'][1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.MuiGrid-root.MuiGrid-container.MuiGrid-direction-xs-column.Settings.css-1lz9gxs > div.MuiGrid-root.css-1oy0n29 > div.MuiGrid-root.css-avuqbz > div.MuiGrid-root.css-rfnosa > span > button.MuiButtonBase-root.MuiIconButton-root.MuiIconButton-sizeMedium.css-g5fp96 > svg.MuiSvgIcon-root.MuiSvgIcon-fontSizeMedium.css-lka7sr</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorCollection>
      <entry>
         <key>SMART_LOCATOR</key>
         <value>div:nth-child(3) > div > div > div:nth-child(2) > span > .MuiButtonBase-root</value>
      </entry>
   </smartLocatorCollection>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>svg</value>
      <webElementGuid>53038bd1-79b0-489b-850d-a297b66a909d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiSvgIcon-root MuiSvgIcon-fontSizeMedium css-lka7sr</value>
      <webElementGuid>7dd3ba23-fdb7-4407-9efc-ba0ceed510a3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>focusable</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>ff433a50-13e9-4ae3-9bb0-112ed6308c77</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-hidden</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>5ef42251-8415-496a-a38a-64d1fbf89142</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>viewBox</name>
      <type>Main</type>
      <value>0 0 24 24</value>
      <webElementGuid>bb7d84ab-aabb-4915-a2a1-13d3b65450a3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-testid</name>
      <type>Main</type>
      <value>ExpandLessIcon</value>
      <webElementGuid>106af339-1162-43e0-b165-86063e8f70b6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;MuiGrid-root MuiGrid-container MuiGrid-direction-xs-column MuiGrid-wrap-xs-nowrap css-5u791s&quot;]/div[@class=&quot;MuiGrid-root MuiGrid-container MuiGrid-direction-xs-column css-1qrgjpa&quot;]/div[@class=&quot;MuiGrid-root MuiGrid-container css-nxpjbh&quot;]/div[@class=&quot;MuiGrid-root MuiGrid-container css-z7c6hm&quot;]/div[@class=&quot;MuiGrid-root MuiGrid-container MuiGrid-direction-xs-column css-k0hwar&quot;]/div[@class=&quot;MuiGrid-root MuiGrid-container css-yt88p2&quot;]/div[@class=&quot;MuiGrid-root MuiGrid-container MuiGrid-direction-xs-column MuiGrid-wrap-xs-nowrap css-1qcw6km&quot;]/div[@class=&quot;MuiGrid-root MuiGrid-container MuiGrid-direction-xs-column Settings  css-1lz9gxs&quot;]/div[@class=&quot;MuiGrid-root css-1oy0n29&quot;]/div[@class=&quot;MuiGrid-root css-avuqbz&quot;]/div[@class=&quot;MuiGrid-root css-rfnosa&quot;]/span[1]/button[@class=&quot;MuiButtonBase-root MuiIconButton-root MuiIconButton-sizeMedium css-g5fp96&quot;]/svg[@class=&quot;MuiSvgIcon-root MuiSvgIcon-fontSizeMedium css-lka7sr&quot;]</value>
      <webElementGuid>bc7e3724-32b0-4855-af1f-db6575ac1eb0</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Settings'])[1]/following::*[name()='svg'][1]</value>
      <webElementGuid>69622b0b-5879-4c4f-93d2-a4f22bf6f5dd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Quick Message'])[1]/following::*[name()='svg'][1]</value>
      <webElementGuid>ddc5b55e-f912-480e-8616-e968efdd8f1a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Enabled'])[1]/preceding::*[name()='svg'][1]</value>
      <webElementGuid>0e39175e-87ba-40cf-b026-a569214a5f47</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
