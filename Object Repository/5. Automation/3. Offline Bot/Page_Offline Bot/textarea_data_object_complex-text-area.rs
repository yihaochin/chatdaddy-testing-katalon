<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>textarea_data_object_complex-text-area</name>
   <tag></tag>
   <elementGuidId>1a5e07e6-86fc-4d72-b384-36695bd95ba7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'complex-text-area' and @placeholder = 'Enter Message Text']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//textarea[@id='complex-text-area']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>/html[@class=&quot;hydrated&quot;]/body[1]/div[@class=&quot;MuiPopover-root MuiModal-root css-jp7szo&quot;]/div[@class=&quot;MuiBackdrop-root MuiBackdrop-invisible MuiModal-backdrop css-esi9ax&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorCollection>
      <entry>
         <key>SMART_LOCATOR</key>
         <value>internal:attr=[placeholder=&quot;Enter Message Text&quot;i]</value>
      </entry>
   </smartLocatorCollection>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>textarea</value>
      <webElementGuid>d2d829b5-0c33-4e74-9561-f3132f31e16c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>complex-text-area</value>
      <webElementGuid>6435985e-eb79-4c78-a2a0-f14509ecac7b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>rows</name>
      <type>Main</type>
      <value>5</value>
      <webElementGuid>561c74f3-6780-4fdd-94d9-142f628acff7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Enter Message Text</value>
      <webElementGuid>a21821c8-d125-4837-b6ca-5262beab8e40</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;complex-text-area&quot;)</value>
      <webElementGuid>46ee312f-11f5-44ff-897f-62d26e936c49</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//textarea[@id='complex-text-area']</value>
      <webElementGuid>f385f87f-7ba0-4280-b8ce-ac6147af7d31</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//textarea</value>
      <webElementGuid>c5c5b3bb-d157-471c-ab1b-8a9819402d17</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//textarea[@id = 'complex-text-area' and @placeholder = 'Enter Message Text']</value>
      <webElementGuid>7eab0184-60aa-4a4f-827b-0f72a1837821</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
