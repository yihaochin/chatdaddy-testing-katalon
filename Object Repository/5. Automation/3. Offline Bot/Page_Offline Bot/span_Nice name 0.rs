<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Nice name 0</name>
   <tag></tag>
   <elementGuidId>cfdcf7cb-5237-4b00-9a31-e42c232faf5a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div[3]/div[3]/div/div/div/div/div/div/div[2]/div/div/div/button/span[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>span.MuiTypography-root.MuiTypography-smallRegular.css-18k148f</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorCollection>
      <entry>
         <key>SMART_LOCATOR</key>
         <value>internal:role=button[name=&quot;account_tree Nice name 0&quot;i]</value>
      </entry>
   </smartLocatorCollection>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>b10f33d1-7c97-4ba8-b4a1-ffdc7abb64fd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiTypography-root MuiTypography-smallRegular css-18k148f</value>
      <webElementGuid>66940fce-977c-477e-86e2-af8ac61ea7b7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Nice name 0</value>
      <webElementGuid>8a1c9f39-b4b9-430a-95c2-4ee54d854bbf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;MuiGrid-root MuiGrid-container MuiGrid-direction-xs-column MuiGrid-wrap-xs-nowrap css-5u791s&quot;]/div[@class=&quot;MuiGrid-root MuiGrid-container MuiGrid-direction-xs-column css-1qrgjpa&quot;]/div[@class=&quot;MuiGrid-root MuiGrid-container css-nxpjbh&quot;]/div[@class=&quot;MuiGrid-root MuiGrid-container css-z7c6hm&quot;]/div[@class=&quot;MuiGrid-root MuiGrid-container MuiGrid-direction-xs-column css-k0hwar&quot;]/div[@class=&quot;MuiGrid-root MuiGrid-container css-yt88p2&quot;]/div[@class=&quot;MuiGrid-root MuiGrid-container MuiGrid-direction-xs-column MuiGrid-wrap-xs-nowrap css-1qcw6km&quot;]/div[@class=&quot;MuiGrid-root MuiGrid-container MuiGrid-direction-xs-column Setup  css-1lz9gxs&quot;]/div[@class=&quot;MuiGrid-root css-1a47weo&quot;]/div[@class=&quot;MuiGrid-root css-cwgml&quot;]/div[@class=&quot;MuiGrid-root css-6m8atf&quot;]/div[@class=&quot;MuiGrid-root css-rfnosa&quot;]/button[@class=&quot;MuiButtonBase-root MuiButton-root MuiButton-outlined MuiButton-outlinedPrimary MuiButton-sizeMedium MuiButton-outlinedSizeMedium MuiButton-colorPrimary MuiButton-root MuiButton-outlined MuiButton-outlinedPrimary MuiButton-sizeMedium MuiButton-outlinedSizeMedium MuiButton-colorPrimary flow-selector-button css-1lbfmfi&quot;]/span[@class=&quot;MuiTypography-root MuiTypography-smallRegular css-18k148f&quot;]</value>
      <webElementGuid>e8dd7cfa-7fc7-4918-ab6d-5cdbcefafca5</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div[3]/div[3]/div/div/div/div/div/div/div[2]/div/div/div/button/span[2]</value>
      <webElementGuid>9fcdf3b5-28eb-40e1-8220-28822b5f96b7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='account_tree'])[1]/following::span[1]</value>
      <webElementGuid>86f9fa30-196b-4df2-ae92-829962592209</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Select the message flow you’d like to trigger during offline hours.'])[1]/following::span[3]</value>
      <webElementGuid>a2e4b9b3-113f-478a-8d52-1dba61dbdb13</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='quick_phrases'])[1]/preceding::span[4]</value>
      <webElementGuid>843d29cb-050f-439c-883e-b84efa577799</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Quick Message'])[1]/preceding::span[6]</value>
      <webElementGuid>cdf92938-821d-4d3f-86a9-9334a5edf1ca</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Nice name 0']/parent::*</value>
      <webElementGuid>930811e3-b185-40be-918f-603c8cf40c91</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div[3]/div/div/div/div/div/div/div[2]/div/div/div/button/span[2]</value>
      <webElementGuid>302d450c-2e95-49ec-a4f5-2b3855fabbc2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Nice name 0' or . = 'Nice name 0')]</value>
      <webElementGuid>4af1e0ea-5d95-43ec-b7ce-a52a7166942d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
