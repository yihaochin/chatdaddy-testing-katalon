<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Cancel all following messages schedul_6033e7  () (1)</name>
   <tag></tag>
   <elementGuidId>389bfa12-45db-4999-a416-d18dbb69a1f2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//input[@type='checkbox'])[12]</value>
      </entry>
      <entry>
         <key>CSS</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <smartLocatorCollection>
      <entry>
         <key>SMART_LOCATOR</key>
         <value>div:nth-child(3) > .MuiGrid-root > .MuiSwitch-root > .MuiButtonBase-root > .PrivateSwitchBase-input</value>
      </entry>
   </smartLocatorCollection>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>c5f2d882-5c90-4c8d-86ce-e919f5216342</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>PrivateSwitchBase-input MuiSwitch-input css-1m9pwf3</value>
      <webElementGuid>a5d70927-8725-4dcc-a4d3-938ea4e470d5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>checkbox</value>
      <webElementGuid>4c36e270-57bc-4ae0-b1c0-a52f83ec97af</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;MuiGrid-root MuiGrid-container MuiGrid-direction-xs-column MuiGrid-wrap-xs-nowrap css-5u791s&quot;]/div[@class=&quot;MuiGrid-root MuiGrid-container MuiGrid-direction-xs-column css-1qrgjpa&quot;]/div[@class=&quot;MuiGrid-root MuiGrid-container css-nxpjbh&quot;]/div[@class=&quot;MuiGrid-root MuiGrid-container css-z7c6hm&quot;]/div[@class=&quot;MuiGrid-root MuiGrid-container MuiGrid-direction-xs-column css-k0hwar&quot;]/div[@class=&quot;MuiGrid-root MuiGrid-container css-yt88p2&quot;]/div[@class=&quot;MuiGrid-root MuiGrid-container MuiGrid-direction-xs-column MuiGrid-wrap-xs-nowrap css-1qcw6km&quot;]/div[@class=&quot;MuiGrid-root MuiGrid-container MuiGrid-direction-xs-column AdvancedSettings  css-1lz9gxs&quot;]/div[@class=&quot;MuiGrid-root css-1a47weo&quot;]/div[@class=&quot;MuiGrid-root MuiGrid-container css-ojfjw8&quot;]/div[@class=&quot;MuiGrid-root css-cwgml&quot;]/div[@class=&quot;MuiGrid-root nlp-reply-keyword-groups css-6m8atf&quot;]/span[@class=&quot;MuiSwitch-root MuiSwitch-sizeMedium css-131x6nu&quot;]/span[@class=&quot;MuiButtonBase-root MuiSwitch-switchBase MuiSwitch-colorPrimary PrivateSwitchBase-root MuiSwitch-switchBase MuiSwitch-colorPrimary css-1x6xwjt&quot;]/input[@class=&quot;PrivateSwitchBase-input MuiSwitch-input css-1m9pwf3&quot;]</value>
      <webElementGuid>864e17d7-5835-4808-9d47-8d2462ae2e55</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//input[@type='checkbox'])[12]</value>
      <webElementGuid>0df857b3-79dc-4933-a26e-7a432a3c452b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div[3]/div[3]/div/div/div/div/div/div[3]/div[2]/div/div[3]/div/span/span/input</value>
      <webElementGuid>8007fb40-010d-4ba3-8c81-afb71b988020</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/span/span/input</value>
      <webElementGuid>815434c2-3839-411e-9c3d-9a5f77dfff97</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'checkbox']</value>
      <webElementGuid>6ce17439-ad9e-4bae-b4b7-d32310a4a3e4</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
