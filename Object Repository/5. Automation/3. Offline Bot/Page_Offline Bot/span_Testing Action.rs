<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Testing Action</name>
   <tag></tag>
   <elementGuidId>b0ec03f2-28f0-4b96-84e2-80d597f95a12</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='flow-selector-scrollable-target']/div/div/div/div/div/div[2]/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>span.MuiTypography-root.MuiTypography-smallMedium.css-htupfn</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorCollection>
      <entry>
         <key>SMART_LOCATOR</key>
         <value>internal:text=&quot;Testing Action&quot;i</value>
      </entry>
   </smartLocatorCollection>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>161fef72-542d-481d-b7ff-102483c15772</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiTypography-root MuiTypography-smallMedium css-htupfn</value>
      <webElementGuid>38211fe8-3bd1-49df-a60d-ef626984f7f7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Testing Action </value>
      <webElementGuid>95f6f307-60f5-4865-b354-8bac35c9cc82</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Testing Action </value>
      <webElementGuid>bc457622-8aeb-4909-bd8c-81b1dc954e6c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;flow-selector-scrollable-target&quot;)/div[@class=&quot;MuiGrid-root css-17u6x1c&quot;]/div[@class=&quot;MuiGrid-root css-ee5rk7&quot;]/div[@class=&quot;MuiGrid-root css-1ozgfun&quot;]/div[@class=&quot;MuiGrid-root MuiGrid-direction-xs-column css-ofhft&quot;]/div[@class=&quot;MuiGrid-root css-18mqftf&quot;]/div[@class=&quot;MuiGrid-root css-qshgr5&quot;]/span[@class=&quot;MuiTypography-root MuiTypography-smallMedium css-htupfn&quot;]</value>
      <webElementGuid>b06f8af3-32bb-4c86-b1a7-f5e62e1e4662</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='flow-selector-scrollable-target']/div/div/div/div/div/div[2]/span</value>
      <webElementGuid>98028582-3152-4498-b6e8-e01dcbf167b6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Message Flows'])[1]/following::span[1]</value>
      <webElementGuid>d64da72f-d258-48dd-be88-19d55c9fa48a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='​'])[16]/following::span[2]</value>
      <webElementGuid>3a94f2c6-2562-4456-8d74-9cfdc19df779</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Test 1010 (5)'])[1]/preceding::span[1]</value>
      <webElementGuid>5b691bdf-f5ef-4ad7-8002-8daa7effa3e1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Test 1010 (3)ggfdgfdggdfgggggggggggggggggggggggggggggggggggggggggggggg'])[1]/preceding::span[2]</value>
      <webElementGuid>395dee08-ab19-4aa7-8421-cabb491707af</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Testing Action']/parent::*</value>
      <webElementGuid>08d206da-cedc-4329-8f9c-57a604d23e95</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div/div/div/div/div[2]/span</value>
      <webElementGuid>19ef066a-d86f-4a82-b554-2c1c42cd08fd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[@title = 'Testing Action ' and (text() = 'Testing Action ' or . = 'Testing Action ')]</value>
      <webElementGuid>d117c080-fbd7-4b1d-88ce-2a8f1f654e38</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
