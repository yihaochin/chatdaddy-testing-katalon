<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Offline Bot will only be triggered wh_a80e7a</name>
   <tag></tag>
   <elementGuidId>5efe8c12-1237-4ec1-88dd-42924a178a9d</elementGuidId>
   <imagePath>Screenshots/Targets/Object Repository/Page_Offline Bot/input_Offline Bot will only be triggered wh_a80e7a.png</imagePath>
   <selectorCollection>
      <entry>
         <key>IMAGE</key>
         <value>Screenshots/Targets/Object Repository/Page_Offline Bot/input_Offline Bot will only be triggered wh_a80e7a.png</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'PrivateSwitchBase-input MuiSwitch-input css-1m9pwf3' and @type = 'checkbox']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//input[@type='checkbox'])[13]</value>
      </entry>
      <entry>
         <key>CSS</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <smartLocatorCollection>
      <entry>
         <key>SMART_LOCATOR</key>
         <value>div:nth-child(4) > .MuiGrid-root > .MuiSwitch-root > .MuiButtonBase-root > .PrivateSwitchBase-input</value>
      </entry>
   </smartLocatorCollection>
   <smartLocatorEnabled>true</smartLocatorEnabled>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>d7c36798-c920-4019-9ce6-4c1f58512046</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>PrivateSwitchBase-input MuiSwitch-input css-1m9pwf3</value>
      <webElementGuid>3c2acbf1-5ed6-4320-ac63-1d6d15a56d61</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>checkbox</value>
      <webElementGuid>154ddc5f-52ab-4601-8dff-ba19ae62c9b8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;MuiGrid-root MuiGrid-container MuiGrid-direction-xs-column MuiGrid-wrap-xs-nowrap css-5u791s&quot;]/div[@class=&quot;MuiGrid-root MuiGrid-container MuiGrid-direction-xs-column css-1qrgjpa&quot;]/div[@class=&quot;MuiGrid-root MuiGrid-container css-nxpjbh&quot;]/div[@class=&quot;MuiGrid-root MuiGrid-container css-z7c6hm&quot;]/div[@class=&quot;MuiGrid-root MuiGrid-container MuiGrid-direction-xs-column css-k0hwar&quot;]/div[@class=&quot;MuiGrid-root MuiGrid-container css-yt88p2&quot;]/div[@class=&quot;MuiGrid-root MuiGrid-container MuiGrid-direction-xs-column MuiGrid-wrap-xs-nowrap css-1qcw6km&quot;]/div[@class=&quot;MuiGrid-root MuiGrid-container MuiGrid-direction-xs-column AdvancedSettings  css-1lz9gxs&quot;]/div[@class=&quot;MuiGrid-root css-1a47weo&quot;]/div[@class=&quot;MuiGrid-root MuiGrid-container css-ojfjw8&quot;]/div[@class=&quot;MuiGrid-root css-cwgml&quot;]/div[@class=&quot;MuiGrid-root css-6m8atf&quot;]/span[@class=&quot;MuiSwitch-root MuiSwitch-sizeMedium css-131x6nu&quot;]/span[@class=&quot;MuiButtonBase-root MuiSwitch-switchBase MuiSwitch-colorPrimary PrivateSwitchBase-root MuiSwitch-switchBase MuiSwitch-colorPrimary css-1x6xwjt&quot;]/input[@class=&quot;PrivateSwitchBase-input MuiSwitch-input css-1m9pwf3&quot;]</value>
      <webElementGuid>077722db-a7a6-441c-984a-36e57ea05735</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//input[@type='checkbox'])[13]</value>
      <webElementGuid>f655ff37-b811-46ea-8676-0ff0dd012b68</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div[3]/div[3]/div/div/div/div/div/div[3]/div[2]/div/div[4]/div/span/span/input</value>
      <webElementGuid>a72c75dc-e85e-4c23-8b96-2a781b5865e3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/span/span/input</value>
      <webElementGuid>3df0a947-7cfe-4d0e-bb55-5ae8d0c307fe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'checkbox']</value>
      <webElementGuid>8a6c6f42-2b11-46d7-9dd2-8305852ab5f9</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
