<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Test Contact 2 (2)</name>
   <tag></tag>
   <elementGuidId>bead0476-6bc8-4a94-94eb-84e1bef33646</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>span.MuiTypography-root.MuiTypography-smallSemiBold.css-y0u5ii</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Last Edited'])[1]/following::span[3]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorCollection>
      <entry>
         <key>SMART_LOCATOR</key>
         <value>internal:text=&quot;Test Contact 2 (2)&quot;i</value>
      </entry>
   </smartLocatorCollection>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>35c66cea-7df8-405b-9334-b392f965239c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiTypography-root MuiTypography-smallSemiBold css-y0u5ii</value>
      <webElementGuid>1ac412ea-72bc-4b1e-a751-d83f885672f5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Test Contact 2 (2)</value>
      <webElementGuid>5f439b04-90e9-4bbe-9282-00d1c049cae3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;hydrated&quot;]/body[1]/div[@class=&quot;base-Popper-root css-0&quot;]/div[@class=&quot;MuiPaper-root MuiPaper-25 MuiPaper-rounded css-w1vxwg&quot;]/div[@class=&quot;MuiGrid-root css-jqp7hk&quot;]/div[1]/div[@class=&quot;complex-text-area-dropdown&quot;]/div[1]/div[@class=&quot;MuiGrid-root MuiGrid-container MuiGrid-direction-xs-column MuiGrid-wrap-xs-nowrap css-1dkt4xm&quot;]/span[@class=&quot;MuiTypography-root MuiTypography-smallSemiBold css-y0u5ii&quot;]</value>
      <webElementGuid>4624babd-b55a-4893-bbc3-cf7c19032956</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Last Edited'])[1]/following::span[3]</value>
      <webElementGuid>2433810a-4661-4edc-8189-6c7962652fe0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sorted By:'])[1]/following::span[4]</value>
      <webElementGuid>27ac98b8-ec68-4a47-9b0f-973f80175682</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='beta may 14'])[1]/preceding::span[2]</value>
      <webElementGuid>7a944228-1c84-4b48-a365-4fb46177a4de</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Test Contact 2 (2)']/parent::*</value>
      <webElementGuid>04d750e9-d928-4604-b8d1-743da779ae27</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div[2]/div/div/span</value>
      <webElementGuid>fc23a27f-4041-4cc5-bea0-eb48019489bf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Test Contact 2 (2)' or . = 'Test Contact 2 (2)')]</value>
      <webElementGuid>bb94be4d-08d6-4421-925f-807129ac1f9f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
