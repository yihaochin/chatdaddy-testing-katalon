<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Payment Reminder NotificationHello Than_97f31a_1</name>
   <tag></tag>
   <elementGuidId>12ab0278-9a0e-4b7f-92f9-3f509d2e3fbb</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='3EB0CDFED7F746B3D4A|1-content']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorCollection>
      <entry>
         <key>SMART_LOCATOR</key>
         <value>[id=&quot;\33 EB0CDFED7F746B3D4A\|1-content&quot;]</value>
      </entry>
   </smartLocatorCollection>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>e7b9b515-3b01-413a-aace-0e2dbd701645</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiGrid-root css-hitvrn</value>
      <webElementGuid>3e073e02-bd15-40a1-98b6-2eb38ebb4b93</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>3EB0CDFED7F746B3D4A|1-content</value>
      <webElementGuid>6b499540-baf2-4c58-8ae5-9b7ebaab32cb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>🔥💲*Payment Reminder Notification*

Hello! 

Thank you for your purchase 😀

=========

 you have an outstanding order for:

💰*Total: HKD 10*

=========

We'd like to remind you that you can make your payment through scanning the QR Code or the link/s below.

 🙆‍♂️🙇‍♀️Please don't hesitate to reach out to us if you are facing any issues with the payments and don't forget to send us the proof of payment. Thanks!🙇‍♂️🙏Testing demoMay 17, 2024 at 5:59 PMdone_all</value>
      <webElementGuid>318162eb-f989-4864-9206-afbd77e08d8e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;3EB0CDFED7F746B3D4A|1-content&quot;)</value>
      <webElementGuid>3a27bca4-3615-4e68-be6e-96aa3a1ef502</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='3EB0CDFED7F746B3D4A|1-content']</value>
      <webElementGuid>ef7b2768-d190-450e-a65a-1273f33af357</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='3EB0CDFED7F746B3D4A|1']/div</value>
      <webElementGuid>70364b1b-597b-4257-b86a-48afbaf1de4a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='done_all'])[18]/following::div[3]</value>
      <webElementGuid>53653805-a2fd-41ac-8714-08e27c65ee1c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='May 17, 2024 at 5:56 PM'])[1]/following::div[3]</value>
      <webElementGuid>1138be17-ae76-41f0-bbd2-a3839cb25000</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[16]/div/div</value>
      <webElementGuid>fea8fe5b-62f2-40d8-a69c-d0a275d50e65</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@id = '3EB0CDFED7F746B3D4A|1-content' and (text() = concat(&quot;🔥💲*Payment Reminder Notification*

Hello! 

Thank you for your purchase 😀

=========

 you have an outstanding order for:

💰*Total: HKD 10*

=========

We&quot; , &quot;'&quot; , &quot;d like to remind you that you can make your payment through scanning the QR Code or the link/s below.

 🙆‍♂️🙇‍♀️Please don&quot; , &quot;'&quot; , &quot;t hesitate to reach out to us if you are facing any issues with the payments and don&quot; , &quot;'&quot; , &quot;t forget to send us the proof of payment. Thanks!🙇‍♂️🙏Testing demoMay 17, 2024 at 5:59 PMdone_all&quot;) or . = concat(&quot;🔥💲*Payment Reminder Notification*

Hello! 

Thank you for your purchase 😀

=========

 you have an outstanding order for:

💰*Total: HKD 10*

=========

We&quot; , &quot;'&quot; , &quot;d like to remind you that you can make your payment through scanning the QR Code or the link/s below.

 🙆‍♂️🙇‍♀️Please don&quot; , &quot;'&quot; , &quot;t hesitate to reach out to us if you are facing any issues with the payments and don&quot; , &quot;'&quot; , &quot;t forget to send us the proof of payment. Thanks!🙇‍♂️🙏Testing demoMay 17, 2024 at 5:59 PMdone_all&quot;))]</value>
      <webElementGuid>f768aaf0-7405-4ec5-b5e2-9f9783569af6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='3EB0CDFED5ED5CDB3DF|1-content']</value>
      <webElementGuid>4bc10c16-6e7b-4d41-a095-f626be88277f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='3EB0CDFED5ED5CDB3DF|1']/div</value>
      <webElementGuid>d4680c75-4298-4112-a52f-c2c251491587</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='done_all'])[17]/following::div[3]</value>
      <webElementGuid>2d007a7a-df50-4c85-b051-f3eaa51b8d4f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='May 17, 2024 at 5:51 PM'])[1]/following::div[3]</value>
      <webElementGuid>8350d759-b018-46eb-a9ee-f0a6d03d5a63</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@id = '3EB0CDFED5ED5CDB3DF|1-content' and (text() = concat(&quot;🔥💲*Payment Reminder Notification*

Hello! 

Thank you for your purchase 😀

=========

 you have an outstanding order for:

💰*Total: HKD 10*

=========

We&quot; , &quot;'&quot; , &quot;d like to remind you that you can make your payment through scanning the QR Code or the link/s below.

 🙆‍♂️🙇‍♀️Please don&quot; , &quot;'&quot; , &quot;t hesitate to reach out to us if you are facing any issues with the payments and don&quot; , &quot;'&quot; , &quot;t forget to send us the proof of payment. Thanks!🙇‍♂️🙏Testing demoMay 17, 2024 at 5:56 PMdone_all&quot;) or . = concat(&quot;🔥💲*Payment Reminder Notification*

Hello! 

Thank you for your purchase 😀

=========

 you have an outstanding order for:

💰*Total: HKD 10*

=========

We&quot; , &quot;'&quot; , &quot;d like to remind you that you can make your payment through scanning the QR Code or the link/s below.

 🙆‍♂️🙇‍♀️Please don&quot; , &quot;'&quot; , &quot;t hesitate to reach out to us if you are facing any issues with the payments and don&quot; , &quot;'&quot; , &quot;t forget to send us the proof of payment. Thanks!🙇‍♂️🙏Testing demoMay 17, 2024 at 5:56 PMdone_all&quot;))]</value>
      <webElementGuid>7f804c58-de14-466e-b93a-58dbf0cbc19a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
