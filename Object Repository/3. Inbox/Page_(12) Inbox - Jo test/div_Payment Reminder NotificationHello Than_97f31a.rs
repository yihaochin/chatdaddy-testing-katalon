<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Payment Reminder NotificationHello Than_97f31a</name>
   <tag></tag>
   <elementGuidId>eb06846d-86c6-4869-8472-a1709b9e79c3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='3EB0CDFED7F746B3D4A|1-content']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorCollection>
      <entry>
         <key>SMART_LOCATOR</key>
         <value>[id=&quot;\33 EB0CDFED7F746B3D4A\|1-content&quot;]</value>
      </entry>
   </smartLocatorCollection>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>f9b23ad9-b9e4-419e-a334-a72c1b37e6ef</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiGrid-root css-hitvrn</value>
      <webElementGuid>654249f3-9800-4bd4-bd38-bc316d3a3a48</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>3EB0CDFED7F746B3D4A|1-content</value>
      <webElementGuid>a525ed76-36ef-4bcd-b9fa-8123b6da2b5e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>🔥💲*Payment Reminder Notification*

Hello! 

Thank you for your purchase 😀

=========

 you have an outstanding order for:

💰*Total: HKD 10*

=========

We'd like to remind you that you can make your payment through scanning the QR Code or the link/s below.

 🙆‍♂️🙇‍♀️Please don't hesitate to reach out to us if you are facing any issues with the payments and don't forget to send us the proof of payment. Thanks!🙇‍♂️🙏Testing demoMay 17, 2024 at 5:59 PMdone</value>
      <webElementGuid>5c4825ff-2ca6-4523-a8dd-f1e47ba640cf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;3EB0CDFED7F746B3D4A|1-content&quot;)</value>
      <webElementGuid>ef61588a-ea1a-4977-9fc7-ce29d73bd8c0</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='3EB0CDFED7F746B3D4A|1-content']</value>
      <webElementGuid>5b4dfdd9-b44e-4e80-8d7a-c5d97f01e00d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='3EB0CDFED7F746B3D4A|1']/div</value>
      <webElementGuid>cbfd5291-d8f1-48be-8850-bebb9f52af63</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='done_all'])[17]/following::div[3]</value>
      <webElementGuid>8cfcc762-4878-4cf7-9664-9e38f1f1a470</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='May 17, 2024 at 5:56 PM'])[1]/following::div[3]</value>
      <webElementGuid>f58ae8ed-d447-4ac9-b294-4408ef8d68bd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[16]/div/div</value>
      <webElementGuid>2fed4633-3a0b-4b54-b044-ecbc6bc1144b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@id = '3EB0CDFED7F746B3D4A|1-content' and (text() = concat(&quot;🔥💲*Payment Reminder Notification*

Hello! 

Thank you for your purchase 😀

=========

 you have an outstanding order for:

💰*Total: HKD 10*

=========

We&quot; , &quot;'&quot; , &quot;d like to remind you that you can make your payment through scanning the QR Code or the link/s below.

 🙆‍♂️🙇‍♀️Please don&quot; , &quot;'&quot; , &quot;t hesitate to reach out to us if you are facing any issues with the payments and don&quot; , &quot;'&quot; , &quot;t forget to send us the proof of payment. Thanks!🙇‍♂️🙏Testing demoMay 17, 2024 at 5:59 PMdone&quot;) or . = concat(&quot;🔥💲*Payment Reminder Notification*

Hello! 

Thank you for your purchase 😀

=========

 you have an outstanding order for:

💰*Total: HKD 10*

=========

We&quot; , &quot;'&quot; , &quot;d like to remind you that you can make your payment through scanning the QR Code or the link/s below.

 🙆‍♂️🙇‍♀️Please don&quot; , &quot;'&quot; , &quot;t hesitate to reach out to us if you are facing any issues with the payments and don&quot; , &quot;'&quot; , &quot;t forget to send us the proof of payment. Thanks!🙇‍♂️🙏Testing demoMay 17, 2024 at 5:59 PMdone&quot;))]</value>
      <webElementGuid>cd22c512-c676-45fd-9c66-819763a24899</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='3EB0CDFED5ED5CDB3DF|1-content']</value>
      <webElementGuid>807efe0a-2996-4f67-be19-235fba99c46c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='3EB0CDFED5ED5CDB3DF|1']/div</value>
      <webElementGuid>5d3f17ff-156e-4258-96db-c393ec1d83ff</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='done_all'])[16]/following::div[3]</value>
      <webElementGuid>027fede9-72ca-4a5d-aa39-f92724310d98</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='May 17, 2024 at 5:51 PM'])[1]/following::div[3]</value>
      <webElementGuid>bcb97461-23e9-4828-b5cb-3f83e3600b32</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@id = '3EB0CDFED5ED5CDB3DF|1-content' and (text() = concat(&quot;🔥💲*Payment Reminder Notification*

Hello! 

Thank you for your purchase 😀

=========

 you have an outstanding order for:

💰*Total: HKD 10*

=========

We&quot; , &quot;'&quot; , &quot;d like to remind you that you can make your payment through scanning the QR Code or the link/s below.

 🙆‍♂️🙇‍♀️Please don&quot; , &quot;'&quot; , &quot;t hesitate to reach out to us if you are facing any issues with the payments and don&quot; , &quot;'&quot; , &quot;t forget to send us the proof of payment. Thanks!🙇‍♂️🙏Testing demoMay 17, 2024 at 5:56 PMdone&quot;) or . = concat(&quot;🔥💲*Payment Reminder Notification*

Hello! 

Thank you for your purchase 😀

=========

 you have an outstanding order for:

💰*Total: HKD 10*

=========

We&quot; , &quot;'&quot; , &quot;d like to remind you that you can make your payment through scanning the QR Code or the link/s below.

 🙆‍♂️🙇‍♀️Please don&quot; , &quot;'&quot; , &quot;t hesitate to reach out to us if you are facing any issues with the payments and don&quot; , &quot;'&quot; , &quot;t forget to send us the proof of payment. Thanks!🙇‍♂️🙏Testing demoMay 17, 2024 at 5:56 PMdone&quot;))]</value>
      <webElementGuid>e7a9e9e4-cb69-4a86-93e4-c2bc580ddddd</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
