<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>p_Ivan Bentley</name>
   <tag></tag>
   <elementGuidId>a1a6f8c9-4fe5-4a57-9126-0ace58993e0e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button.MuiButtonBase-root.MuiButton-root.MuiButton-text.MuiButton-textPrimary.MuiButton-sizeMedium.MuiButton-textSizeMedium.MuiButton-colorPrimary.MuiButton-root.MuiButton-text.MuiButton-textPrimary.MuiButton-sizeMedium.MuiButton-textSizeMedium.MuiButton-colorPrimary.css-1q6ge0y > p.MuiTypography-root.MuiTypography-body1.css-adhdfs</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='inbox-header']/div[2]/div/button/p</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorCollection>
      <entry>
         <key>SMART_LOCATOR</key>
         <value>#inbox-header >> internal:role=button[name=&quot;Ivan Bentley expand_more&quot;i]</value>
      </entry>
   </smartLocatorCollection>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
      <webElementGuid>19c34d20-cf24-4481-baaa-e2958d261e86</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiTypography-root MuiTypography-body1 css-adhdfs</value>
      <webElementGuid>8f1b1b6a-ce48-4f22-8aba-63420e382253</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Ivan Bentley</value>
      <webElementGuid>62780871-84ae-49af-b6d9-c88270a64529</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;inbox-header&quot;)/div[@class=&quot;MuiGrid-root css-fqgfhr&quot;]/div[@class=&quot;MuiGrid-root css-yzfd7p&quot;]/button[@class=&quot;MuiButtonBase-root MuiButton-root MuiButton-text MuiButton-textPrimary MuiButton-sizeMedium MuiButton-textSizeMedium MuiButton-colorPrimary MuiButton-root MuiButton-text MuiButton-textPrimary MuiButton-sizeMedium MuiButton-textSizeMedium MuiButton-colorPrimary css-1q6ge0y&quot;]/p[@class=&quot;MuiTypography-root MuiTypography-body1 css-adhdfs&quot;]</value>
      <webElementGuid>0c58ca5c-58db-4312-a5cc-1c31b84f7b97</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='inbox-header']/div[2]/div/button/p</value>
      <webElementGuid>315f9144-f2c8-409f-ae84-35088984d7dc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='call_received'])[1]/following::p[2]</value>
      <webElementGuid>069d0f58-3979-453f-b819-e0d82f301d41</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='call_made'])[1]/following::p[3]</value>
      <webElementGuid>4894e385-81c6-4258-b166-4a7c6a026278</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='expand_more'])[10]/preceding::p[1]</value>
      <webElementGuid>b95a9b8b-d64d-47fa-8621-88ccc0a9ef2a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Close Ticket'])[1]/preceding::p[1]</value>
      <webElementGuid>934d8603-b4c1-4bd4-bf05-15a52ebc33f5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div[2]/div/button/p</value>
      <webElementGuid>1b1d3bee-81d5-41a8-9f37-16cbfd1adc5c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//p[(text() = 'Ivan Bentley' or . = 'Ivan Bentley')]</value>
      <webElementGuid>7a295f32-de7e-412d-8459-2541fa94b1db</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='expand_more'])[11]/preceding::p[1]</value>
      <webElementGuid>bdb04c8c-e3d3-49d3-bd5a-0ba78b8a2e36</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
