<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Touch n Go</name>
   <tag></tag>
   <elementGuidId>baddd1d0-4aca-43e7-9e20-01be7c89c325</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.MuiGrid-root.css-5vqq3p</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Create Pay Request'])[1]/following::div[3]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorCollection>
      <entry>
         <key>SMART_LOCATOR</key>
         <value>internal:label=&quot;Touch 'n Go&quot;i</value>
      </entry>
   </smartLocatorCollection>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>1d5700ac-f538-440b-b514-d38cce272426</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiGrid-root css-5vqq3p</value>
      <webElementGuid>c8796093-5108-413e-bb2a-aec26e813853</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-label</name>
      <type>Main</type>
      <value>Touch 'n Go</value>
      <webElementGuid>e7ae54c3-04ec-44ec-8197-1d0aa9d53097</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Touch 'n Go</value>
      <webElementGuid>8343767c-bcad-4ba8-a809-d12e0011d779</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;hydrated&quot;]/body[1]/div[@class=&quot;MuiDialog-root MuiModal-root css-126xj0f&quot;]/div[@class=&quot;MuiDialog-container MuiDialog-scrollPaper css-ekeie0&quot;]/div[@class=&quot;MuiPaper-root MuiPaper-elevation MuiPaper-rounded MuiPaper-elevation24 MuiDialog-paper MuiDialog-paperScrollPaper MuiDialog-paperWidthSm css-uhb5lp&quot;]/div[@class=&quot;MuiGrid-root css-1l9mz8y&quot;]/div[@class=&quot;MuiGrid-root css-pxp2ko&quot;]/div[@class=&quot;MuiGrid-root css-rt6km9&quot;]/div[@class=&quot;MuiGrid-root css-5vqq3p&quot;]</value>
      <webElementGuid>6746f6cc-8409-4415-8eac-6faae9da3dee</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Create Pay Request'])[1]/following::div[3]</value>
      <webElementGuid>12e3e3c8-3380-486e-923d-2cd5f2d0e0ad</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Drop Files Here'])[1]/following::div[31]</value>
      <webElementGuid>110ab2b7-167d-4288-a43a-d6950dc9916a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Stripe'])[1]/preceding::div[1]</value>
      <webElementGuid>bf8f1893-fc82-4c37-b946-12d430da9d77</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[6]/div[3]/div/div/div[2]/div/div</value>
      <webElementGuid>77e0eaa1-8e22-4605-883e-cef63dc145cd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = concat(&quot;Touch &quot; , &quot;'&quot; , &quot;n Go&quot;) or . = concat(&quot;Touch &quot; , &quot;'&quot; , &quot;n Go&quot;))]</value>
      <webElementGuid>88a259e8-0512-46d4-9061-daf63c2d25e7</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
