<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_test buttonbutton10button20Testing demo_d0f3d9</name>
   <tag></tag>
   <elementGuidId>f95ccafa-39e7-4b80-9c6c-9a87fce202c4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='3EB0CDFED01FAF0F4F7|1-content']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorCollection>
      <entry>
         <key>SMART_LOCATOR</key>
         <value>internal:text=&quot;test buttonbutton10button20Testing demoMay 17, 2024 at 4:12 PMdone&quot;i</value>
      </entry>
   </smartLocatorCollection>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>0fd4bae8-dba4-4365-bd45-0353984e86a8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiGrid-root css-hitvrn</value>
      <webElementGuid>cc3d61a0-eef3-4566-8e32-e7c44510da4b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>3EB0CDFED01FAF0F4F7|1-content</value>
      <webElementGuid>18f2b369-22d9-4f49-b9ee-3b22ce00874c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>test buttonbutton10button20Testing demoMay 17, 2024 at 4:12 PMdone</value>
      <webElementGuid>4e870cc8-fa4a-4b57-92ec-389aaa424b04</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;3EB0CDFED01FAF0F4F7|1-content&quot;)</value>
      <webElementGuid>f2623083-2d53-4fcf-b6c3-899cc3bd4497</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='3EB0CDFED01FAF0F4F7|1-content']</value>
      <webElementGuid>5a1c2e44-eaef-49ee-81f6-12b6493bbb63</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='3EB0CDFED01FAF0F4F7|1']/div</value>
      <webElementGuid>7b51e8da-6e1c-4e5c-877e-bd3bc051c210</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='done_all'])[13]/following::div[3]</value>
      <webElementGuid>37240b3a-11a5-4d76-831a-4dd3ee8f9c68</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='May 17, 2024 at 4:09 PM'])[1]/following::div[3]</value>
      <webElementGuid>897be2ce-f235-4977-b9f0-fe6bcd9ae0f1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[16]/div/div</value>
      <webElementGuid>c5a2e188-96e6-45f0-9d6d-afbb2d574bf5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@id = '3EB0CDFED01FAF0F4F7|1-content' and (text() = 'test buttonbutton10button20Testing demoMay 17, 2024 at 4:12 PMdone' or . = 'test buttonbutton10button20Testing demoMay 17, 2024 at 4:12 PMdone')]</value>
      <webElementGuid>1334e0c5-dbe3-40dc-9c9a-fe2859a9927c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
