<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_test signaturesignaturesTesting demoMay_b93080</name>
   <tag></tag>
   <elementGuidId>6f78baf7-ee6c-4fad-b461-af12e7cfbce6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='3EB0CDFED479EC50C6C|1-content']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorCollection>
      <entry>
         <key>SMART_LOCATOR</key>
         <value>internal:text=&quot;test signature signaturesTesting demoMay 17, 2024 at 5:50 PMdone&quot;i</value>
      </entry>
   </smartLocatorCollection>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>3dddd910-4b82-4400-a389-8d70e40b631e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiGrid-root css-hitvrn</value>
      <webElementGuid>d826033a-a7b1-4195-b06c-52ed777ed0d1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>3EB0CDFED479EC50C6C|1-content</value>
      <webElementGuid>7c6cf101-3ab6-469f-af86-ea9ba0e66e21</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>test signature

signaturesTesting demoMay 17, 2024 at 5:50 PMdone</value>
      <webElementGuid>3c9216c6-31ce-4a9d-9d3c-9c842a08ad85</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;3EB0CDFED479EC50C6C|1-content&quot;)</value>
      <webElementGuid>2082b1ef-aba5-4936-a3bb-24b249550111</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='3EB0CDFED479EC50C6C|1-content']</value>
      <webElementGuid>46586a02-615d-4f06-acde-740748f288e6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='3EB0CDFED479EC50C6C|1']/div</value>
      <webElementGuid>c07eff28-36e4-45db-be50-06ffbdc01402</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='done_all'])[15]/following::div[3]</value>
      <webElementGuid>64078a19-ba8e-4fce-a376-7235b0cccda9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='May 17, 2024 at 5:41 PM'])[1]/following::div[3]</value>
      <webElementGuid>0c211e3f-54d7-49f1-9a1f-04f9ebb51fe3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[16]/div/div</value>
      <webElementGuid>9fdcacbc-14ac-41fc-b75f-fe878756f54a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@id = '3EB0CDFED479EC50C6C|1-content' and (text() = 'test signature

signaturesTesting demoMay 17, 2024 at 5:50 PMdone' or . = 'test signature

signaturesTesting demoMay 17, 2024 at 5:50 PMdone')]</value>
      <webElementGuid>f7686396-daf4-4bda-bef7-316f25f333ee</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
