<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>textarea_add_complex-text-area</name>
   <tag></tag>
   <elementGuidId>9d4510a9-d64a-4a5b-a62b-082d4a3a481f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#complex-text-area</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//textarea[@id='complex-text-area']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorCollection>
      <entry>
         <key>SMART_LOCATOR</key>
         <value>internal:role=button[name=&quot;ChatDaddy AI add Create a private note for you and your teammates. Select any piece of text to apply AI modifications to it send Drop Files Here&quot;i]</value>
      </entry>
   </smartLocatorCollection>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>textarea</value>
      <webElementGuid>d2485575-c08d-4e0c-87cc-8def3b45e596</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>complex-text-area</value>
      <webElementGuid>d24717fc-1909-4f9b-8ea7-1640f49bb84b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Create a private note for you and your teammates. Select any piece of text to apply AI modifications to it</value>
      <webElementGuid>1a5ecda1-82a3-4d7e-b4b9-5fbc07574899</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;complex-text-area&quot;)</value>
      <webElementGuid>b0266653-8162-42b2-ada2-8c7b06964985</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//textarea[@id='complex-text-area']</value>
      <webElementGuid>f540a106-cece-4603-bade-812d4c8cfd9f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div[3]/div[3]/div/div[3]/div[2]/div/div[2]/div[2]/div/div/div[2]/div/div/textarea</value>
      <webElementGuid>b1654868-fd8a-4414-8a82-dfae69a36a60</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//textarea</value>
      <webElementGuid>83ce7bd9-f17f-41e4-9c8a-b017854519b3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//textarea[@id = 'complex-text-area' and @placeholder = 'Create a private note for you and your teammates. Select any piece of text to apply AI modifications to it']</value>
      <webElementGuid>ea0cf29b-743a-4014-89e2-3c39fefd4811</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//textarea[@id = 'complex-text-area' and @placeholder = concat(&quot;Type &quot; , &quot;'&quot; , &quot;/&quot; , &quot;'&quot; , &quot; to start a Message Flow! To apply AI modifications to your text, select it!&quot;)]</value>
      <webElementGuid>fbe9ab69-1873-4176-b875-fd36f5b7955b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
