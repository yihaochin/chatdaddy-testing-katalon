<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_test signaturesignatureTesting demoMay _1a517c</name>
   <tag></tag>
   <elementGuidId>295197d6-eb94-474e-9e32-2f9454a9138a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='3EB0CDFEDC5AF15EE83|1-content']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorCollection>
      <entry>
         <key>SMART_LOCATOR</key>
         <value>internal:text=&quot;test signature signatureTesting demoMay 17, 2024 at 5:41 PMdone&quot;i</value>
      </entry>
   </smartLocatorCollection>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>75dfe312-69c3-47e5-a331-ae4a07ec862f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiGrid-root css-hitvrn</value>
      <webElementGuid>3f90f4fc-c7e7-4192-bee3-1ab1845638bd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>3EB0CDFEDC5AF15EE83|1-content</value>
      <webElementGuid>13d2ef46-99d9-475e-80e5-6ec748aea9e9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>test signature

signatureTesting demoMay 17, 2024 at 5:41 PMdone</value>
      <webElementGuid>b660680e-319a-43a9-a7d9-eeab2402156a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;3EB0CDFEDC5AF15EE83|1-content&quot;)</value>
      <webElementGuid>20f11490-c58c-4efd-a4c5-d7ce4b14835e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='3EB0CDFEDC5AF15EE83|1-content']</value>
      <webElementGuid>962b9365-8bba-4f3b-bfe9-67b78769c6f0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='3EB0CDFEDC5AF15EE83|1']/div</value>
      <webElementGuid>d02b1857-e089-4cd7-9913-01dd03b79a94</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='done_all'])[15]/following::div[3]</value>
      <webElementGuid>72552d61-6ec4-4cb9-b7e6-2708ffa80ed6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='May 17, 2024 at 4:36 PM'])[1]/following::div[3]</value>
      <webElementGuid>439bf67e-d68f-45cf-a233-61511b3910ec</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[16]/div/div</value>
      <webElementGuid>a0d08b21-cedf-45f8-97ad-ffdf6b00c1e9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@id = '3EB0CDFEDC5AF15EE83|1-content' and (text() = 'test signature

signatureTesting demoMay 17, 2024 at 5:41 PMdone' or . = 'test signature

signatureTesting demoMay 17, 2024 at 5:41 PMdone')]</value>
      <webElementGuid>8619cd15-a668-436d-861d-53a1dd936b31</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
