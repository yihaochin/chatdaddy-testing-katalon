<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_test signaturesignaturesTesting demoMay_1aaa93</name>
   <tag></tag>
   <elementGuidId>f68101d8-3274-4e64-9836-f33cbe718ef7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='3EB0CDFED479EC50C6C|1-content']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorCollection>
      <entry>
         <key>SMART_LOCATOR</key>
         <value>internal:text=&quot;test signature signaturesTesting demoMay 17, 2024 at 5:50 PMdone_all&quot;i</value>
      </entry>
   </smartLocatorCollection>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>adccc84e-4dae-4762-b8f5-96767dafe086</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiGrid-root css-hitvrn</value>
      <webElementGuid>d4d10141-d308-4a3e-ba32-d9cb9b16ff92</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>3EB0CDFED479EC50C6C|1-content</value>
      <webElementGuid>99ad5af8-90e6-4801-af67-a66712fa828f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>test signature

signaturesTesting demoMay 17, 2024 at 5:50 PMdone_all</value>
      <webElementGuid>b0902db9-f336-42c4-9375-834869718acb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;3EB0CDFED479EC50C6C|1-content&quot;)</value>
      <webElementGuid>6b2d83ad-0fda-42a8-aa01-9d06730c52c6</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='3EB0CDFED479EC50C6C|1-content']</value>
      <webElementGuid>70f2038c-5df8-45fa-bfb9-58c09fc930a2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='3EB0CDFED479EC50C6C|1']/div</value>
      <webElementGuid>cdce853c-892c-4811-bfc4-b00c7bed3174</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='done_all'])[16]/following::div[3]</value>
      <webElementGuid>9fc05a96-50dc-47f0-ab8e-ad316b783a15</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='May 17, 2024 at 5:41 PM'])[1]/following::div[3]</value>
      <webElementGuid>3ed79d65-3b88-4eaa-adb5-fb033c1cde61</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[16]/div/div</value>
      <webElementGuid>8ae9fc55-e5c0-47cb-afc9-6ff853307019</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@id = '3EB0CDFED479EC50C6C|1-content' and (text() = 'test signature

signaturesTesting demoMay 17, 2024 at 5:50 PMdone_all' or . = 'test signature

signaturesTesting demoMay 17, 2024 at 5:50 PMdone_all')]</value>
      <webElementGuid>c494482e-cac3-43d7-95fc-2a688a94f25e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
