<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_test signaturesignatureTesting demoMay _75f1bc</name>
   <tag></tag>
   <elementGuidId>828b7244-a10d-43fe-8ab8-bf1314a9e618</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='3EB0CDFEDC5AF15EE83|1-content']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorCollection>
      <entry>
         <key>SMART_LOCATOR</key>
         <value>internal:text=&quot;test signature signatureTesting demoMay 17, 2024 at 5:41 PMdone_all&quot;i</value>
      </entry>
   </smartLocatorCollection>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>412d428e-7cfb-4482-bed0-dedb20a2086e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiGrid-root css-hitvrn</value>
      <webElementGuid>2eb7953a-7b90-4f60-bb72-1410bdef16b5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>3EB0CDFEDC5AF15EE83|1-content</value>
      <webElementGuid>15336610-4fe4-47aa-ac39-28233dd5b780</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>test signature

signatureTesting demoMay 17, 2024 at 5:41 PMdone_all</value>
      <webElementGuid>85c02620-ca22-4b5b-b9c2-e5b6a61cf073</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;3EB0CDFEDC5AF15EE83|1-content&quot;)</value>
      <webElementGuid>bb9d66b0-cb9c-4ab3-baf3-1fe2a8bb0bcd</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='3EB0CDFEDC5AF15EE83|1-content']</value>
      <webElementGuid>8cddffc6-1edc-4f41-888d-8c068233a31f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='3EB0CDFEDC5AF15EE83|1']/div</value>
      <webElementGuid>a41332f1-abed-4240-9bd4-85a39d20a321</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='done_all'])[16]/following::div[3]</value>
      <webElementGuid>2e95ad4a-e0a5-4917-add0-291b3de30d1b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='May 17, 2024 at 4:36 PM'])[1]/following::div[3]</value>
      <webElementGuid>7e59f7ee-3420-422c-a5c5-674672dbec16</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[16]/div/div</value>
      <webElementGuid>7acb745e-6d89-46b8-865b-e9a706768b6e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@id = '3EB0CDFEDC5AF15EE83|1-content' and (text() = 'test signature

signatureTesting demoMay 17, 2024 at 5:41 PMdone_all' or . = 'test signature

signatureTesting demoMay 17, 2024 at 5:41 PMdone_all')]</value>
      <webElementGuid>c279f071-d578-4b10-9fba-dab6d50587a6</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
