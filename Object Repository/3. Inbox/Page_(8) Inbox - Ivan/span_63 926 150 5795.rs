<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_63 926 150 5795</name>
   <tag></tag>
   <elementGuidId>9dadccad-6a2b-4abd-a50e-7761d470118e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>span.MuiTypography-root.MuiTypography-xSmallRegular.css-17psejy</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='inbox-header']/div[2]/div/div/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorCollection>
      <entry>
         <key>SMART_LOCATOR</key>
         <value>#inbox-header >> internal:attr=[title=&quot;+63 926 150 5795&quot;i]</value>
      </entry>
   </smartLocatorCollection>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>0958d162-2d73-44a8-90d7-9f055abd50fe</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiTypography-root MuiTypography-xSmallRegular css-17psejy</value>
      <webElementGuid>b2685c88-b806-425d-be71-2db9beb759f7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>+63 926 150 5795</value>
      <webElementGuid>0db9dbd6-6026-435a-97bd-08d8f3cf7904</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>+63 926 150 5795</value>
      <webElementGuid>100d8ca9-f9e2-4bc2-a997-d3260f41304a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;inbox-header&quot;)/div[@class=&quot;MuiGrid-root css-fqgfhr&quot;]/div[@class=&quot;MuiGrid-root css-yzfd7p&quot;]/div[@class=&quot;MuiGrid-root css-c1vp97&quot;]/span[@class=&quot;MuiTypography-root MuiTypography-xSmallRegular css-17psejy&quot;]</value>
      <webElementGuid>242dd11d-8208-461d-8f24-c71b3fd10715</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='inbox-header']/div[2]/div/div/span</value>
      <webElementGuid>4c8d4d2e-2760-4eeb-8c4b-effe7b662de6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='expand_more'])[10]/following::span[2]</value>
      <webElementGuid>21f8687b-9780-4bd9-8d72-b09d182aa779</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='call_received'])[1]/following::span[4]</value>
      <webElementGuid>a9bc9cb6-e3be-467e-bb2b-1f6002012aa1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Open Ticket'])[1]/preceding::span[1]</value>
      <webElementGuid>d0cc4556-55cc-4aa9-9a80-b2e17aec37a3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='search'])[2]/preceding::span[2]</value>
      <webElementGuid>0ea58425-ca60-4e83-8f71-b8c975ad8bef</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div[2]/div/div/span</value>
      <webElementGuid>f0c73cec-3bc8-40d8-a81e-c8cefcf95e58</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[@title = '+63 926 150 5795' and (text() = '+63 926 150 5795' or . = '+63 926 150 5795')]</value>
      <webElementGuid>8cf4f943-335c-4c1a-a049-d35029043a25</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
