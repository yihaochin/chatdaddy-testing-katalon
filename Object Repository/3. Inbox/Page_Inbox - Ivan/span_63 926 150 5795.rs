<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_63 926 150 5795</name>
   <tag></tag>
   <elementGuidId>2d9e22ba-d16f-4b00-8853-098cdb590123</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>span.MuiTypography-root.MuiTypography-xSmallRegular.css-19cfj96</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='inbox-header']/div/div/div[2]/span[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorCollection>
      <entry>
         <key>SMART_LOCATOR</key>
         <value>internal:text=&quot;+63 926 150 5795&quot;i >> nth=1</value>
      </entry>
   </smartLocatorCollection>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>cfd04a3d-5564-49a3-b1fe-8981a653310c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiTypography-root MuiTypography-xSmallRegular css-19cfj96</value>
      <webElementGuid>d56488c4-0e22-4085-8c8b-a25d31b2b2a3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>+63 926 150 5795</value>
      <webElementGuid>745ce7e5-b6fb-4e16-a10f-3085d79261fd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;inbox-header&quot;)/div[@class=&quot;MuiGrid-root css-u2a8x6&quot;]/div[@class=&quot;MuiGrid-root tour-contact-info css-7bcfnq&quot;]/div[@class=&quot;MuiGrid-root css-1m444xx&quot;]/span[@class=&quot;MuiTypography-root MuiTypography-xSmallRegular css-19cfj96&quot;]</value>
      <webElementGuid>1dd812ab-55d0-4623-927c-e5ddac994c1f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='inbox-header']/div/div/div[2]/span[2]</value>
      <webElementGuid>61e2593e-040f-46ed-ab50-d9cc5b0d554d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='phone'])[1]/following::span[1]</value>
      <webElementGuid>9a876e1c-ee2c-4b2b-8f1e-a4f90ffe6686</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='edit'])[1]/following::span[3]</value>
      <webElementGuid>2f135353-d351-4c50-902b-23258a5ab0fa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='content_copy'])[1]/preceding::span[1]</value>
      <webElementGuid>f1e15319-ca7a-4563-9ec2-3dd2bc7f3f00</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='call_made'])[1]/preceding::span[3]</value>
      <webElementGuid>721a4b9d-2859-46fd-b596-02a944bb4239</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/span[2]</value>
      <webElementGuid>d5bfecbd-65b0-4275-8a77-8c683aade24a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = '+63 926 150 5795' or . = '+63 926 150 5795')]</value>
      <webElementGuid>5c624e99-5a0c-4c5d-af14-53ec27a12374</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
