<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>p_Sorry We couldnt find the chat you searched for</name>
   <tag></tag>
   <elementGuidId>f5658388-e694-4701-92a9-3c233c5474f7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>p.MuiTypography-root.MuiTypography-body2.css-9pm7v1</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div[3]/div[3]/div/div[2]/div[3]/p</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorCollection>
      <entry>
         <key>SMART_LOCATOR</key>
         <value>internal:text=&quot;Sorry! We couldn’t find the chat you searched for.&quot;i</value>
      </entry>
   </smartLocatorCollection>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
      <webElementGuid>5b2e8037-7af1-4bb9-84ac-1e9757a81b88</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiTypography-root MuiTypography-body2 css-9pm7v1</value>
      <webElementGuid>1bbda8fa-3769-4a8f-8ee1-b078f2e56d43</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Sorry! We couldn’t find the chat you searched for.</value>
      <webElementGuid>f9ccd0bb-f500-41ee-960b-2c8c4b95075d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;MuiGrid-root MuiGrid-container MuiGrid-direction-xs-column MuiGrid-wrap-xs-nowrap css-5u791s&quot;]/div[@class=&quot;MuiGrid-root css-qrdc5g&quot;]/div[@class=&quot;MuiGrid-root css-1r3ro9p&quot;]/div[@class=&quot;MuiGrid-root css-148h8t7&quot;]/div[@class=&quot;MuiGrid-root css-12u48ha&quot;]/p[@class=&quot;MuiTypography-root MuiTypography-body2 css-9pm7v1&quot;]</value>
      <webElementGuid>ac9e93db-1ff1-4946-88c9-e5015799ea1d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div[3]/div[3]/div/div[2]/div[3]/p</value>
      <webElementGuid>67aceac9-d74d-49f1-916e-b18d8b35d4db</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Bulk Actions'])[1]/following::p[1]</value>
      <webElementGuid>0f4b3328-e193-43c1-afd1-c445de9f897b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='arrow_drop_down'])[5]/following::p[2]</value>
      <webElementGuid>d7f74bbf-016c-441e-b8e5-7332daa59b34</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Search Messages Instead?'])[1]/preceding::p[1]</value>
      <webElementGuid>44fb67e9-76d7-4c05-9da4-200f4bd2dbc1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Search Messages Instead?'])[2]/preceding::p[1]</value>
      <webElementGuid>9ada0c35-1307-4ffc-a545-686823f59614</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Sorry! We couldn’t find the chat you searched for.']/parent::*</value>
      <webElementGuid>6478d44d-a8eb-4b6e-8f22-9bc9d520b837</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/p</value>
      <webElementGuid>a5231432-bcbd-4415-9e14-7c8b2787ff21</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//p[(text() = 'Sorry! We couldn’t find the chat you searched for.' or . = 'Sorry! We couldn’t find the chat you searched for.')]</value>
      <webElementGuid>b508d5aa-4fcd-47a3-a042-efa2354dfbc5</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
