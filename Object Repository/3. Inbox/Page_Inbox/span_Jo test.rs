<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Jo test</name>
   <tag></tag>
   <elementGuidId>118a2b45-975a-4284-bd6e-9acfd8c25bbc</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>span.MuiTypography-root.MuiTypography-body1.css-1sop232</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div[3]/div[3]/div/div[2]/div[3]/div/div[2]/div/a/div[2]/div/div/span/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorCollection>
      <entry>
         <key>SMART_LOCATOR</key>
         <value>internal:role=link[name=&quot;account_circle Jo test 2h done_all test VIP Jo Broadcast - 30 Apr 24 +1 Ivan Bentley expand_more +63 926 150 5795&quot;i]</value>
      </entry>
   </smartLocatorCollection>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>ab271eb8-80e5-4fad-9e57-a7968b77000c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiTypography-root MuiTypography-body1 css-1sop232</value>
      <webElementGuid>6e05bdaf-94c6-492a-a3ec-d55b2458a497</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Jo test</value>
      <webElementGuid>4f7fd7ba-28e2-43bb-ac9a-571c1e07aefe</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Jo test</value>
      <webElementGuid>da5cd3d7-b4cd-4dd7-8005-13e4c92f4a61</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;MuiGrid-root MuiGrid-container MuiGrid-direction-xs-column MuiGrid-wrap-xs-nowrap css-5u791s&quot;]/div[@class=&quot;MuiGrid-root css-qrdc5g&quot;]/div[@class=&quot;MuiGrid-root css-1r3ro9p&quot;]/div[@class=&quot;MuiGrid-root css-148h8t7&quot;]/div[@class=&quot;MuiGrid-root css-12u48ha&quot;]/div[1]/div[2]/div[1]/a[@class=&quot;MuiGrid-root css-128ea43&quot;]/div[@class=&quot;MuiGrid-root css-1vlsgix&quot;]/div[@class=&quot;MuiGrid-root css-1mr0hd4&quot;]/div[@class=&quot;MuiGrid-root css-1trzygj&quot;]/span[@class=&quot;MuiTypography-root MuiTypography-baseMedium css-1nchl2b&quot;]/span[@class=&quot;MuiTypography-root MuiTypography-body1 css-1sop232&quot;]</value>
      <webElementGuid>8a47fb67-ce28-46a6-8d44-a898d8497de0</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div[3]/div[3]/div/div[2]/div[3]/div/div[2]/div/a/div[2]/div/div/span/span</value>
      <webElementGuid>ff3d6447-e727-4ff2-a962-5b889ce32960</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='account_circle'])[2]/following::span[4]</value>
      <webElementGuid>590e00ed-682f-47ef-bc21-76e356c6fc2a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Bulk Actions'])[1]/following::span[5]</value>
      <webElementGuid>d316d9cb-8231-4696-8c31-442872826925</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='done_all'])[1]/preceding::span[1]</value>
      <webElementGuid>869e2b97-3883-4705-870b-721b98a14d1e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='test'])[1]/preceding::span[2]</value>
      <webElementGuid>e248055a-4527-411b-9d8f-227f56d12f9e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Jo test']/parent::*</value>
      <webElementGuid>47ea85ca-915d-44e0-b004-fce116d41629</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/span/span</value>
      <webElementGuid>c954cd67-e7de-4eaf-a973-ac282036535f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Jo test' or . = 'Jo test')]</value>
      <webElementGuid>0a67cae2-5190-40a5-87b3-7d07db9b9e90</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div[3]/div[3]/div/div[2]/div[3]/div/div[2]/div/a/div[2]/div/div/span</value>
      <webElementGuid>04728b25-9133-47fc-9f9c-3af191574b7a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='account_circle'])[2]/following::span[3]</value>
      <webElementGuid>4dda54db-9f59-4f14-aeaf-3ca9e8f4b02b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Bulk Actions'])[1]/following::span[4]</value>
      <webElementGuid>10f26131-8ab6-4d6c-bc55-dbbd456a544c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='done_all'])[1]/preceding::span[2]</value>
      <webElementGuid>543b7ad9-ae04-4a4c-9633-3453ebcf11ab</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='test'])[1]/preceding::span[3]</value>
      <webElementGuid>d4e88a1a-ae4b-4185-8bea-dd9aa4d5416b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/span</value>
      <webElementGuid>6f28ef63-53f7-439e-8486-c746da38aaa4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[@title = 'Jo test' and (text() = 'Jo test' or . = 'Jo test')]</value>
      <webElementGuid>2681adbf-5253-4d39-83fc-8d31701d7373</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
