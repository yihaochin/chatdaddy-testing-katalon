<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>p_Sorry We couldnt find the chat you searched for</name>
   <tag></tag>
   <elementGuidId>eb7b8e91-0615-4cbc-a9c1-dd5fc931f59d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>p.MuiTypography-root.MuiTypography-body2.css-9pm7v1</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div[3]/div[3]/div/div[2]/div[3]/p</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorCollection>
      <entry>
         <key>SMART_LOCATOR</key>
         <value>internal:text=&quot;Sorry! We couldn’t find the chat you searched for.&quot;i</value>
      </entry>
   </smartLocatorCollection>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
      <webElementGuid>a630f465-c456-4511-b3c3-92e0fc139638</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiTypography-root MuiTypography-body2 css-9pm7v1</value>
      <webElementGuid>5b97731d-1bb2-451d-9cd9-1a3f59a1d6a7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Sorry! We couldn’t find the chat you searched for.</value>
      <webElementGuid>f44f591c-50a1-47ad-bc0f-79e41498e903</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;MuiGrid-root MuiGrid-container MuiGrid-direction-xs-column MuiGrid-wrap-xs-nowrap css-5u791s&quot;]/div[@class=&quot;MuiGrid-root css-qrdc5g&quot;]/div[@class=&quot;MuiGrid-root css-1r3ro9p&quot;]/div[@class=&quot;MuiGrid-root css-148h8t7&quot;]/div[@class=&quot;MuiGrid-root css-12u48ha&quot;]/p[@class=&quot;MuiTypography-root MuiTypography-body2 css-9pm7v1&quot;]</value>
      <webElementGuid>1dc24a7d-45ad-47d9-97ad-412ecd5d8a87</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div[3]/div[3]/div/div[2]/div[3]/p</value>
      <webElementGuid>a8e3f056-9f9e-4899-9a38-5cc21e97131b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Bulk Actions'])[1]/following::p[1]</value>
      <webElementGuid>e9785280-6bed-4909-81a9-bd19fc02b7cc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='arrow_drop_down'])[5]/following::p[2]</value>
      <webElementGuid>71a9afad-9ff8-470e-9704-937b02f9b4d1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Search Messages Instead?'])[1]/preceding::p[1]</value>
      <webElementGuid>36d8f923-ae44-4793-b43c-971276765d66</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Search Messages Instead?'])[2]/preceding::p[1]</value>
      <webElementGuid>2c5f459d-6d0d-4fae-a51c-0fa06b0edc9c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Sorry! We couldn’t find the chat you searched for.']/parent::*</value>
      <webElementGuid>9502bd16-ff72-4d5d-bbdb-b30ee401184e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/p</value>
      <webElementGuid>88b82f68-055c-45cb-88a7-45fd8bd98715</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//p[(text() = 'Sorry! We couldn’t find the chat you searched for.' or . = 'Sorry! We couldn’t find the chat you searched for.')]</value>
      <webElementGuid>2f9dc4bc-65a1-4eb2-abe8-50bfc78a8565</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
