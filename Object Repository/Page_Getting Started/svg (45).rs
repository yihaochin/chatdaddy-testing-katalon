<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>svg (45)</name>
   <tag></tag>
   <elementGuidId>668706df-4d86-4e36-af3e-bb9e50518ef2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;root&quot;)/div[@class=&quot;MuiCollapse-root MuiCollapse-horizontal MuiCollapse-entered css-yhefzq&quot;]/div[@class=&quot;MuiCollapse-wrapper MuiCollapse-horizontal css-164swfl&quot;]/div[@class=&quot;MuiCollapse-wrapperInner MuiCollapse-horizontal css-1mziwkx&quot;]/div[@class=&quot;MuiBox-root css-gxs633&quot;]/div[@class=&quot;MuiGrid-root css-1d2hfx&quot;]/div[@class=&quot;MuiBox-root css-0&quot;]/div[@class=&quot;MuiGrid-root css-wivtdb&quot;]/div[1]/svg[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.MuiBox-root.css-0 > div.MuiGrid-root.css-wivtdb > div > svg</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='fullscreen_exit'])[1]/preceding::*[name()='svg'][8]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>svg</value>
      <webElementGuid>6e301147-70e1-4636-b02c-14ff28675aed</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xmlns</name>
      <type>Main</type>
      <value>http://www.w3.org/2000/svg</value>
      <webElementGuid>f957f514-f48e-4049-8f32-fd6e5afe2cdc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xmlns:xlink</name>
      <type>Main</type>
      <value>http://www.w3.org/1999/xlink</value>
      <webElementGuid>c3be4a84-d8d3-4daa-ad1d-e1343587fd7d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>viewBox</name>
      <type>Main</type>
      <value>0 0 500 500</value>
      <webElementGuid>bde9508a-0887-4875-9cd7-4b5b8a29db5b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>width</name>
      <type>Main</type>
      <value>500</value>
      <webElementGuid>6f4dfffe-7d41-48e3-b28d-e8384360da39</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>height</name>
      <type>Main</type>
      <value>500</value>
      <webElementGuid>7f40da22-7542-4b0c-ae55-c82507221707</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>preserveAspectRatio</name>
      <type>Main</type>
      <value>xMidYMid meet</value>
      <webElementGuid>dad3f8f1-7008-4cb8-a992-691db7ccf389</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;MuiCollapse-root MuiCollapse-horizontal MuiCollapse-entered css-yhefzq&quot;]/div[@class=&quot;MuiCollapse-wrapper MuiCollapse-horizontal css-164swfl&quot;]/div[@class=&quot;MuiCollapse-wrapperInner MuiCollapse-horizontal css-1mziwkx&quot;]/div[@class=&quot;MuiBox-root css-gxs633&quot;]/div[@class=&quot;MuiGrid-root css-1d2hfx&quot;]/div[@class=&quot;MuiBox-root css-0&quot;]/div[@class=&quot;MuiGrid-root css-wivtdb&quot;]/div[1]/svg[1]</value>
      <webElementGuid>697c9f26-6cdd-4aba-9ebc-e03a44b7aff4</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='fullscreen_exit'])[1]/preceding::*[name()='svg'][8]</value>
      <webElementGuid>d75f12a7-7e41-4fe4-b484-5a4d34946884</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
