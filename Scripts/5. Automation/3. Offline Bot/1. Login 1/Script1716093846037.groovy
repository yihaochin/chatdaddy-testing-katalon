import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://www.beta.chatdaddy.tech/Login')

WebUI.setText(findTestObject('5. Automation/3. Offline Bot/Page_ChatDaddy/input__r1'), '1718333267')

WebUI.setEncryptedText(findTestObject('Object Repository/5. Automation/3. Offline Bot/Page_ChatDaddy/input_Password_password'), 'W7ymuD5+KhEjdiCdW+m2WA==')

WebUI.click(findTestObject('Object Repository/5. Automation/3. Offline Bot/Page_ChatDaddy/button_Login'))

WebUI.click(findTestObject('Object Repository/5. Automation/3. Offline Bot/Page_Getting Started/button_Close (1)'))

WebUI.click(findTestObject('Object Repository/5. Automation/3. Offline Bot/Page_Getting Started/svg (10)'))

WebUI.click(findTestObject('5. Automation/3. Offline Bot/Page_Getting Started/span_Offline Bot (1)'))

