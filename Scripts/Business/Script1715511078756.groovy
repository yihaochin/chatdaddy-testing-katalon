import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Dropdown Button functionality'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/5. Automation/3. Offline Bot/Page_Offline Bot/input_Enabled_PrivateSwitchBase-input MuiSw_55fd6b (1)'))

WebUI.click(findTestObject('Object Repository/5. Automation/3. Offline Bot/Page_Offline Bot/input_Enabled_PrivateSwitchBase-input MuiSw_55fd6b (1)'))

WebUI.click(findTestObject('Object Repository/Page_Offline Bot/input_Monday_PrivateSwitchBase-input MuiSwi_398f32 (7)'))

WebUI.click(findTestObject('Object Repository/5. Automation/3. Offline Bot/Page_Offline Bot/input_Tuesday_PrivateSwitchBase-input MuiSw_7f0bef'))

WebUI.click(findTestObject('Object Repository/5. Automation/3. Offline Bot/Page_Offline Bot/input_Wednesday_PrivateSwitchBase-input Mui_46be73'))

WebUI.click(findTestObject('Object Repository/5. Automation/3. Offline Bot/Page_Offline Bot/input_Thursday_PrivateSwitchBase-input MuiS_ea4a36'))

WebUI.click(findTestObject('Object Repository/5. Automation/3. Offline Bot/Page_Offline Bot/input_Friday_PrivateSwitchBase-input MuiSwi_cf5637'))

WebUI.click(findTestObject('Object Repository/5. Automation/3. Offline Bot/Page_Offline Bot/input_Saturday_PrivateSwitchBase-input MuiS_f7a12d'))

WebUI.click(findTestObject('Object Repository/5. Automation/3. Offline Bot/Page_Offline Bot/input_Sunday_PrivateSwitchBase-input MuiSwi_11132e'))

