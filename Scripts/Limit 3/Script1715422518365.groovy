import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Login 1'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('5. Automation/3. Offline Bot/Page_Offline Bot/span_arrow_drop_down (1)'))

WebUI.click(findTestObject('Object Repository/5. Automation/3. Offline Bot/Page_Offline Bot/input_Need Help_PrivateSwitchBase-input css_76dbfd (2)'))

WebUI.sendKeys(findTestObject('Object Repository/5. Automation/3. Offline Bot/Page_Offline Bot/input_Need Help_PrivateSwitchBase-input css_76dbfd (2)'), 
    Keys.chord(Keys.ESCAPE))

not_run: WebUI.click(findTestObject('null'))

not_run: WebUI.click(findTestObject('null'))

not_run: WebUI.click(findTestObject('Object Repository/5. Automation/3. Offline Bot/Page_Offline Bot/input_Seconds_PrivateSwitchBase-input css-1m9pwf3 limit'))

not_run: WebUI.sendKeys(findTestObject('Object Repository/5. Automation/3. Offline Bot/Page_Offline Bot/input_Seconds_PrivateSwitchBase-input css-1m9pwf3 limit'), 
    Keys.chord(Keys.ESCAPE))

not_run: WebUI.click(findTestObject('null'))

