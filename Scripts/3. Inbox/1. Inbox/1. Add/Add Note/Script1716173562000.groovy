import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://www.beta.chatdaddy.tech/login')

WebUI.click(findTestObject('Object Repository/Page_ChatDaddy/button__MuiButtonBase-root MuiIconButton-ro_c0ec4c'))

WebUI.click(findTestObject('Object Repository/Page_ChatDaddy/li_Singapore65'))

WebUI.setText(findTestObject('5. Automation/3. Offline Bot/Page_ChatDaddy/input__r1'), '01234567')

WebUI.setEncryptedText(findTestObject('Object Repository/5. Automation/3. Offline Bot/Page_ChatDaddy/input_Password_password'), 'p4y+y39Ir5MSxNs1t5lTZQ==')

WebUI.sendKeys(findTestObject('Object Repository/5. Automation/3. Offline Bot/Page_ChatDaddy/input_Password_password'), Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('Object Repository/5. Automation/3. Offline Bot/Page_Getting Started/button_Close'))

WebUI.click(findTestObject('Object Repository/Page_Getting Started/svg'))

WebUI.click(findTestObject('Object Repository/Page_Getting Started/span_Inbox'))

WebUI.click(findTestObject('Object Repository/Page_(12) Inbox/span_Skip this Short and Sweet Tour'))

WebUI.click(findTestObject('Object Repository/Page_(12) Inbox/span_Jo test'))

WebUI.click(findTestObject('Object Repository/Page_(12) Inbox - Jo test/span_settings'))

WebUI.click(findTestObject('Object Repository/Page_(12) Inbox - Jo test/input_Add new_PrivateSwitchBase-input css-1m9pwf3'))

WebUI.sendKeys(findTestObject('Object Repository/Page_(12) Inbox - Jo test/input_Add new_PrivateSwitchBase-input css-1m9pwf3'), Keys.chord(Keys.ESCAPE))

WebUI.click(findTestObject('Object Repository/Page_(12) Inbox - Jo test/button_Note'))

WebUI.setText(findTestObject('Object Repository/Page_(12) Inbox - Jo test/textarea_add_complex-text-area'), 'add note')

WebUI.click(findTestObject('Object Repository/Page_(12) Inbox - Jo test/span_send'))

WebUI.click(findTestObject('Object Repository/Page_(12) Inbox - Jo test/span_Jo test'))

WebUI.verifyElementPresent(findTestObject('Object Repository/Page_(12) Inbox - Jo test/div_add note'), 0)

WebUI.closeBrowser()

