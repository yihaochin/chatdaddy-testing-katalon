import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Login 1'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/5. Automation/3. Offline Bot/Page_Offline Bot/svg_Setup_MuiSvgIcon-root MuiSvgIcon-fontSi_513125'))

WebUI.click(findTestObject('Object Repository/5. Automation/3. Offline Bot/Page_Offline Bot/svg_Setup_MuiSvgIcon-root MuiSvgIcon-fontSi_513125'))

WebUI.click(findTestObject('Object Repository/5. Automation/3. Offline Bot/Page_Offline Bot/svg_Settings_MuiSvgIcon-root MuiSvgIcon-fon_f52217'))

WebUI.click(findTestObject('Object Repository/5. Automation/3. Offline Bot/Page_Offline Bot/svg_Settings_MuiSvgIcon-root MuiSvgIcon-fon_f52217'))

WebUI.click(findTestObject('Object Repository/5. Automation/3. Offline Bot/Page_Offline Bot/svg_Advanced Settings_MuiSvgIcon-root MuiSv_7688a7'))

WebUI.click(findTestObject('Object Repository/5. Automation/3. Offline Bot/Page_Offline Bot/svg_Advanced Settings_MuiSvgIcon-root MuiSv_7688a7'))

WebUI.click(findTestObject('Object Repository/5. Automation/3. Offline Bot/Page_Offline Bot/input_Mark Chat as Read if Offline Bot is T_dfbe6f'))

WebUI.click(findTestObject('Object Repository/5. Automation/3. Offline Bot/Page_Offline Bot/input_Respond with Keyword reply in Group C_b0ccc3'))

WebUI.click(findTestObject('Object Repository/5. Automation/3. Offline Bot/Page_Offline Bot/input_Cancel all following messages schedul_6033e7'))

WebUI.click(findTestObject('Object Repository/5. Automation/3. Offline Bot/Page_Offline Bot/input_Offline Bot will only be triggered wh_a80e7a'))

WebUI.click(findTestObject('Object Repository/5. Automation/3. Offline Bot/Page_Offline Bot/div_Save (2)'))

